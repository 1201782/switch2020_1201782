package bloco4;


public class Exercicio5 {
    public static int numerosPares(int x) {
        int somaDosPares = 0;
        for (int i = 0; i <= x; i++) {
            if (i % 2 == 0) {
                somaDosPares = somaDosPares + i;
            }
        }
        return somaDosPares;
    }

    public static int quantidadePares(int x) {
        int contadorDePares = 0;
        for (int i = 0; i <= x; i++) {
            if (i % 2 == 0) {
                contadorDePares++;
            }
        }
        return contadorDePares;
    }

    public static int somaNumerosImpares(int x) {
        int somaDosImpares = 0;
        for (int i = 0; i <= x; i++) {
            if (i % 2 != 0) {
                somaDosImpares = somaDosImpares + i;
            }
        }
        return somaDosImpares;
    }

    public static int quantidadeImpares(int x) {
        int contadorDeImpares = 0;
        for (int i = 0; i <= x; i++) {
            if (i % 2 != 0) {
                contadorDeImpares++;
            }
        }
        return contadorDeImpares;
    }

    public static int somaMultiplos(int x, int y, int z) {
        int soma = 0;
        for (int i = x; i > 0; i--) {
            if (i % y == 0 && i % z == 0) {
                soma = soma + i;
            }
        }
        return soma;
    }

    public static int produtoDosMultiplos(int x, int y) {
        int produto = 0;
        for (int i = 0; i < x; i++) {
            if (i % y == 0) {
            produto = produto +(i*y);
            }
        }
        return produto;
    }

    public static double mediaMultiplos(int x, int y, int z) {
        int soma=0;
        int totalMultiplos=0;
        for (int i = x; i > 0; i--) {
            if (i % y == 0 && i % z == 0) {
                soma = soma + i;
                totalMultiplos++;
            }
        }
        double media = soma/totalMultiplos;
        return media;
    }

    public static int mediaMultiplosH(int x, int y, int z) {
        int soma=0;
        int totalMultiplos=0;
        for (int i = x; i > 0; i--) {
            if (i % y == 0 || i % z == 0) {
                soma = soma + i;
                totalMultiplos++;
            }
        }
        int media = soma/totalMultiplos;
        return media;
    }

}
