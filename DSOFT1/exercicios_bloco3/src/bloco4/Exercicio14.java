package bloco4;

import java.util.Scanner;

public class Exercicio14 {
    public static void main(String[] args) {
        trocarMoeda();
    }

    public static void trocarMoeda() {

        Scanner read = new Scanner(System.in);
        int valor=1;
        while (valor >= 0) {
            System.out.println("Escolha a moeda desejada digitando o seguinte código");
            System.out.println("Para dólar: 1\nPara Libra: 2\nPara Iene: 3\nPara Coroa Sueca: 4\nPara Franco Suíço: 5");
            int moeda = read.nextInt();
            //neste exercício eu poderia pedir ao usuário para inserir um valor em euros e
            // calcular quanto valerá na moeda escolhida. Mas não dá tempo de fazer tudo.
            switch (moeda) {
                case 1:
                    System.out.println("1 euro = 1,534 dólares");
                    break;
                case 2:
                    System.out.println("1 euro = 0,774 libras");
                    break;
                case 3:
                    System.out.println("1 euro = 161,480 ienes");
                    break;
                case 4:
                    System.out.println("1 euro = 9,593 coroas sueca");
                    break;
                case 5:
                    System.out.println("1 euro = 1,601 francos suíços");
                    break;
                default:
                    System.out.println("Inválido");
                    break;
            }
            System.out.println("Para sair digite 1, para continuar digite 0");
            int sair = read.nextInt();
            if (sair==1){ //não faz sentido o usuário digitar um valor negativo
                valor=-1;
            }
        }
    }
}
