package bloco4;

import java.util.Scanner;

public class Exercicio17 {
    public static void main(String[] args) {
        validarQuantRacaoAnimal();
    }

    public static void validarQuantRacaoAnimal() {
        Scanner read = new Scanner(System.in);
        boolean continuar = true;
        while (continuar) {
            System.out.println("Digite o peso do animal");
            int pesoAnimal = read.nextInt();
            if (pesoAnimal <= 0) {
                continuar = false;
                break;
            }

            System.out.println("Digite a quantidade em gramas que o animal come");
            int quantidadeRacao = read.nextInt();

            if (pesoAnimal <= 10) {
                if (quantidadeRacao <= 100) {
                    System.out.println("Quantidade de ração correta");
                } else {
                    System.out.println("Quantidade de ração incorreta, raça pequena deve comer uma média de 100g");
                }
            } else if (pesoAnimal > 10 && pesoAnimal <= 25) {
                if (quantidadeRacao <= 250) {
                    System.out.println("Quantidade de ração correta");
                } else {
                    System.out.println("Quantidade de ração incorreta, raça média deve comer uma média de 250g");
                }
            } else if (pesoAnimal > 25 && pesoAnimal <= 45) {
                if (quantidadeRacao <= 300) {
                    System.out.println("Quantidade de ração correta");
                } else {
                    System.out.println("Quantidade de ração incorreta, raça grande deve comer uma média de 300g");
                }

            } else {
                if (quantidadeRacao <= 500) {
                    System.out.println("Quantidade de ração correta");
                } else {
                    System.out.println("Quantidade de ração incorreta, raça gigante deve comer uma média de 500g");
                }

            }
        }
    }
}
