package bloco4;

public class Exercicio1 {

    public static int cicloFor1(int num) {
        int res = 1;
        int x;

        for (x = num; x >= 1; x--) {
            res = res * x;
        }
        return res;
    }

}
