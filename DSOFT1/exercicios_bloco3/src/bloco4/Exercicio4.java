package bloco4;

public class Exercicio4 {

    public static int multiplosDeTres(int x) {
        int contadorDeMultiplos = 0;
        for (int i = 1; i <= x; i++) {
            if (i % 3 == 0) {
                contadorDeMultiplos++;
            }
        }
        return contadorDeMultiplos;
    }

    public static int multiplosDoisNumeros(int x, int y) {
        int contadorDeMultiplos = 0;
        for (int i = 1; i <= x; i++) {
            if (i % y == 0) {
                contadorDeMultiplos++;
            }
        }
        return contadorDeMultiplos;
    }

    public static int multiplosDeTresECinco(int x) {
        int contadorDeMultiplos = 0;
        for (int i = 1; i <= x; i++) {
            if (i % 3 == 0 && i %5==0) {
                contadorDeMultiplos++;
            }
        }
        return contadorDeMultiplos;
    }
    public static int multiplosDoisNumerosAleatorios(int x, int y, int z) {
        int contadorDeMultiplos = 0;
        for (int i = 1; i <= x; i++) {
            if (i % y == 0 && i %z == 0) {
                contadorDeMultiplos++;
            }
        }
        return contadorDeMultiplos;
    }
    public static int somaMultiplos(int x, int y, int z) {
        int soma=0;
        for (int i = 0; i <= x; i++) {
            if (i % y == 0 && i % z == 0) {
                soma = soma + i;
            }
        }
        return soma;
    }

}
