package bloco4;

public class Exercicio20 {

    public static String categorizarNumeros(int numero) {
        //perfeito - n= soma dos divisores excluindo n
        //reduzidos - n = maior que a soma dos divisores excluindo n
        //abundantes - n = menor que a soma dos divisores inclusive n

        String categoria = "";
        int somaNumero = 0;

        for (int i = 1; i <= numero; i++) {
            if (numero % i == 0) {
                somaNumero += i;
            }
        }
        if ((somaNumero-numero) == numero) {
            categoria = "Perfeito";
        } else if ((somaNumero-numero) < numero) {
            categoria = "Reduzido";
        } else if (somaNumero > numero) {
            categoria = "Abundante";
        }

        return"A categoria é ".concat(categoria);
    }
}


