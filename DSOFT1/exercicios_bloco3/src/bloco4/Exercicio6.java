package bloco4;

public class Exercicio6 {

    public static int numeroDeAlgarismos(int numero) {
        //descobrir a quantidade de algarismos
        int contador = 0;
        while (numero > 0) {
            numero = numero / 10;
            contador++;
        }
        return contador;
    }

    public static int algarismosPares(int numero) {
        int quantNumPares = 0;
        while (numero > 0) {
            if (numero % 2 == 0) {
                quantNumPares++;
            }
            numero = numero / 10;
        }
        return quantNumPares;
    }

    public static int algarismosImpares(int numero) {
        int quantNumImpares = 0;
        while (numero > 0) {
            if (numero % 2 != 0) {
                quantNumImpares++;
            }
            numero = numero / 10;
        }
        return quantNumImpares;
    }

    public static int somaAlgarismos(int numero) {
        int soma = 0;
        while (numero > 0) {
            int digito = numero % 10;
            soma += digito;
            numero = numero / 10;
        }
        return soma;
    }

    public static int somaAlgarismosPares(int numero) {
        int somaPares = 0;
        while (numero > 0) {
            int digito = numero % 10;
            if (digito % 2 == 0) {
                somaPares += digito;
            }
            numero = numero / 10;
        }
        return somaPares;
    }

    public static int somaAlgarismosImpares(int numero) {
        int somaImpares = 0;
        while (numero > 0) {
            int digito = numero % 10;
            if (digito % 2 != 0) {
                somaImpares += digito;
            }
            numero = numero / 10;
        }
        return somaImpares;
    }

    public static int mediaAlgarismos(int numero) {
        int contador = 0;
        int soma = 0;
        while (numero > 0) {
            int digito = numero % 10;
            soma += digito;
            numero = numero / 10;
            contador++;
        }
        int media = soma / contador;
        return media;
    }

    public static int mediaAlgarismosPares(int numero) {
        int contador = 0;
        int somaPares = 0;
        while (numero > 0) {
            int digito = numero % 10;
            if (digito % 2 == 0) {
                somaPares += digito;
                contador++;
            }
            numero = numero / 10;
        }
        int mediaPares = somaPares / contador;
        return mediaPares;
    }

    public static int mediaAlgarismosImpares(int numero) {
        int contador = 0;
        int somaImpares = 0;
        while (numero > 0) {
            int digito = numero % 10;
            if (digito % 2 != 0) {
                somaImpares += digito;
                contador++;
            }
            numero = numero / 10;
        }
        int mediaImpares = somaImpares / contador;
        return mediaImpares;
    }

    public static String ordemInversa(int numero) {
        String ordemInversa="";
        while (numero > 0) {
            int digito = numero % 10;
            ordemInversa += digito;
            numero = numero / 10;
        }
        return ordemInversa;
    }

}