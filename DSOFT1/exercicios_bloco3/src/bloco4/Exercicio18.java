package bloco4;

import java.util.Scanner;

public class Exercicio18 {

    public static void main(String[] args) {
        verificaBI();
    }

    public static void verificaBI() {
        Scanner read = new Scanner(System.in);

        System.out.println("Digite seu número do BI ou CC");
        int numero = read.nextInt();
        int soma =0;
        int digito;
        for (int i = 1; i <=9 ; i++) {
            digito = (numero %10);
            soma+=(digito*i);
            numero /= 10;
        }

        if (soma % 11 == 0) {
            System.out.println("Número valido");
        } else {
            System.out.println("Número inválido.Digite novamente");
        }

    }
}
