package bloco4;

import java.util.Scanner;

public class Exercicio15 {
    public static void main(String[] args) {
        avaliarNota();
    }
    public static void avaliarNota(){
        Scanner read = new Scanner(System.in);
        boolean addStudent = true;

        while (addStudent) {
            System.out.println("Digite a nota do aluno");
            int nota = read.nextInt();
            if(nota>=0 && nota<=4){
                System.out.println("Mau");
            }else if(nota>=5 && nota<=9){
                System.out.println("Medíocre");
            }else if(nota>=10 && nota<=13){
                System.out.println("Suficiente");
            }else if(nota>=14 && nota<=17){
                System.out.println("Bom");
            }else {
                System.out.println("Muito bom");
            }

            System.out.println("Deseja adicionar outro aluno?(s/n)");
            String resposta = read.next();//era pra inserir uma nota negativa se quisesse sair, mas não faz sentido para mim.
            if(resposta.equals("n")||resposta.equals("N")){
                addStudent=false;
            }
        }
    }

}


