package bloco4;

import java.util.Scanner;

public class Exercicio16 {
    public static void main(String[] args) {
        calculaSalarioLiquido();
    }
    public static void calculaSalarioLiquido(){
        Scanner read = new Scanner(System.in);
        boolean repetir = true;
        do{
        System.out.println("Digite o salário do trabalhador");
        int salario = read.nextInt();
        double salarioLiquido;
        if(salario<=500){
            salarioLiquido = salario - (0.1*salario);
            System.out.println("O salário líquido é de: "+ salarioLiquido);
        }else if(salario>501 && salario<=1000){
            salarioLiquido = salario -(0.15*salario);
            System.out.println("O salário líquido é de: "+salarioLiquido);
        }else {
            salarioLiquido = salario -(0.2*salario);
            System.out.println("O salário líquido é de: "+salarioLiquido);
        }
            System.out.println("Deseja adicionar mais um trabalhador?(s/n)");
            String resposta = read.next();
            if(resposta.equals("n")||resposta.equals("N")){
                repetir=false;
            }
        }while (repetir);
    }

}
