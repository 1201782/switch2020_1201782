package bloco4;

public class Exercicio7 {

    public static boolean eCapicua(int numero) {
        int numeroDeDigitos = contadorDigitos(numero);
        String digito = "";
        boolean eCapicua;
        if (numeroDeDigitos % 2 == 0) {
            for (int i = 0; i < (numeroDeDigitos / 2); i++) {
                digito = digito + (numero % 10);
                numero = numero / 10;
            }
            if (numero == Integer.parseInt(digito)) {
                eCapicua = true;
            } else {
                eCapicua = false;
            }
        } else {
            for (int i = 0; i < (numeroDeDigitos / 2); i++) {
                digito += (numero % 10);
                numero = numero / 10;
            }
            numero = numero / 10;
            if (numero == Integer.parseInt(digito)) {
                eCapicua = true;
            } else {
                eCapicua = false;
            }
        }
        return eCapicua;
    }

    public static int contadorDigitos(int numero) {
        int contador = 0;
        while (numero > 0) {
            numero = numero / 10;
            contador++;
        }
        return contador;
    }

    public static boolean eArmstrong(int numero) {
        int potencia = contadorDigitos(numero);
        int numeroComparativo = numero;
        boolean eArmstrong;
        int digito = 0;
        for (int i = 0; i < potencia; i++) {
            digito = (int) (digito + Math.pow((numeroComparativo % 10), potencia));
            numeroComparativo = numeroComparativo / 10;
        }
        if (digito == numero) {
            return eArmstrong = true;
        } else {
            return eArmstrong = false;
        }
    }

    public static int primeiraCapicua(int intervaloMin, int intervaloMax) {
        int primeiraCapicua = 0;
        for (int i = intervaloMin; i <= intervaloMax; i++) {
            boolean verificacao = eCapicua(i);
            if (verificacao) {
                primeiraCapicua = i;
                i=intervaloMax+1;
            }
        }
        return primeiraCapicua;
    }

    public static int ultimaCapicua(int intervaloMin, int intervaloMax) {
        int maiorCapicua = 0;
        for (int i = intervaloMin; i <= intervaloMax; i++) {
            boolean verificacao = eCapicua(i);
            if (verificacao) {
                maiorCapicua = i;
            }
        }
        return maiorCapicua;
    }

    public static int quantidadeCapicuas(int intervaloMin, int intervaloMax) {
        int capicuaPoint = 0;
        int contador=0;
        for (int i = intervaloMin; i <= intervaloMax; i++) {
            boolean verificacao = eCapicua(i);
            if (verificacao) {
                capicuaPoint = i;
                contador++;
            }
        }
        return contador;
    }

    public static int primeiroArmstrong(int intervaloMin, int intervaloMax){
        int numArmstrong = 0;
        for (int i = intervaloMin; i <= intervaloMax ; i++) {
           boolean testNumeros = eArmstrong(i);
           if(testNumeros){
               numArmstrong = i;
               i=intervaloMax+1;
           }
        }
        return numArmstrong;
    }

    public static int quantidadeDeArmstrongs(int intervaloMin, int intervaloMax){
        int contador=0;
        for (int i = intervaloMin; i <= intervaloMax; i++) {
            eArmstrong(i);
            if (eArmstrong(i)){
                contador ++;
            }
        }
        return contador;
    }
}
