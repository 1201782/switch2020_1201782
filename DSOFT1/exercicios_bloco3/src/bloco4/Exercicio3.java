package bloco4;

import java.util.Scanner;

public class Exercicio3 {
    public static void main(String[] args) {
        sequenciaNumeros();
    }
    public static void sequenciaNumeros(){
        //ler uma sequencia de números
        Scanner read = new Scanner(System.in);

        int quantPares=0; //total de números pares
        int quantImpares=0; //total de números ímpares
        double sumImpares=0; //soma números ímpares

        boolean i=true;
        while (i){
            System.out.println("Digite o primeiro número");
            int primeiroNumero = read.nextInt();
            if(primeiroNumero%2 == 0){
                quantPares++;
            }else{
                quantImpares++;
                sumImpares= sumImpares+ primeiroNumero;
            }
            System.out.println("Digite o segundo número");
            int segundoNumero = read.nextInt();
            if(segundoNumero%2 == 0){
                quantPares++;
            }else{
                quantImpares++;
                sumImpares= sumImpares+segundoNumero;
            }
            System.out.println("Digite o terceiro número");
            int terceiroNumero = read.nextInt();
            if(-(terceiroNumero%2) == 0){
                quantPares++;
            }else{
                quantImpares++;
                sumImpares= sumImpares + terceiroNumero;
            }
            int percentagemPares = (quantPares*100)/3;
            double mediaImpares = sumImpares/quantImpares;
            if(quantImpares == 0){
                mediaImpares=0;
            }

            System.out.println("A percentagem de números pares é " + percentagemPares + "%");
            System.out.println("A média de numeros ímpares é " + mediaImpares);

            System.out.println("Deseja parar?(S/N)");
            String stop = read.next();
            if(stop.equals("s")||stop.equals("S")){
                i=false;
            }else{
                quantPares=0;
                quantImpares=0;
                sumImpares=0;
            }
        }
    }

}
