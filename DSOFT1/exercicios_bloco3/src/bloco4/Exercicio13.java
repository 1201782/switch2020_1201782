package bloco4;


import java.util.Scanner;

public class Exercicio13 {
    public static void main(String[] args) {
        classificaProdutos();
    }
    public static void classificaProdutos() {
        Scanner read = new Scanner(System.in);
        boolean resposta = true;
        while(resposta) {

            System.out.println("Digite o código do produto");
            int codigo = read.nextInt();
            switch (codigo) {
                case 1:
                    System.out.println("Alimento não perecível");
                    break;
                case 2:
                    System.out.println("Alimento perecível");
                    break;
                case 3:
                    System.out.println("Alimento perecível");
                    break;
                case 4:
                    System.out.println("Alimento perecível");
                    break;
                case 5:
                    System.out.println("Vestuário");
                    break;
                case 6:
                    System.out.println("Vestuário");
                    break;
                case 7:
                    System.out.println("Higiene pessoal");
                    break;
                case 8:
                    System.out.println("Limpeza e utensílios domésticos");
                    break;
                case 9:
                    System.out.println("Limpeza e utensílios domésticos");
                    break;
                case 10:
                    System.out.println("Limpeza e utensílios domésticos");
                    break;
                case 11:
                    System.out.println("Limpeza e utensílios domésticos");
                    break;
                case 12:
                    System.out.println("Limpeza e utensílios domésticos");
                    break;
                case 13:
                    System.out.println("Limpeza e utensílios domésticos");
                    break;
                case 14:
                    System.out.println("Limpeza e utensílios domésticos");
                    break;
                case 15:
                    System.out.println("Limpeza e utensílios domésticos");
                    break;

                default:
                    System.out.println("Código inválido");
            }
            System.out.println("Deseja verificar outro código?");
            String verifica = read.next();
            if(verifica.equals("n")||verifica.equals("N")){
                resposta=false;
            }
        }
    }
}
