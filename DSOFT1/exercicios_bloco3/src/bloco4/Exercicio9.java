package bloco4;

import java.util.Scanner;

public class Exercicio9 {

    public static void main(String[] args) {
        salarioFuncionarios();
    }

    public static void salarioFuncionarios() {
        Scanner read = new Scanner(System.in);

        double salarioFinal = 0;
        double somaSalarios = 0;
        int totalFuncionarios = 0;
        boolean adicionarFuncionario = true;
        boolean validador = true;
        int horasExtras;
        int salarioBase;

        while (adicionarFuncionario) {

            do {
                System.out.println("Digite as horas extras do funcionário");
                horasExtras = read.nextInt();

                if (horasExtras > 0) {
                    if (horasExtras < 8) {
                        horasExtras--; //a primeira hora extra não é contabilizada}
                        validador = false;
                    } else {
                        System.out.println("Erro. Um funcionário não pode fazer mais que 8 horas extras");
                    }
                } else {
                    System.out.println("Erro. Digite um número válido");
                }

            } while (validador);

            //ler salário base do funcionário
            System.out.println("Digite o salário base do funcionário");
            salarioBase = read.nextInt();

            salarioFinal = salarioBase + (salarioBase * 0.02) * (horasExtras);
            System.out.println("O salário será: " + salarioFinal + " eur");

            totalFuncionarios++;
            somaSalarios = somaSalarios + salarioFinal;
            do {
                System.out.println("Deseja adicionar mais um funcionário?(S/N)"); // para adicionar funcionários
                String resposta = read.next();

                if (resposta.equals("n") || resposta.equals("N")) {
                    adicionarFuncionario = false;
                } else {
                    System.out.println("Digite novamente. Digite S para sim ou N para não");
                }
            } while (adicionarFuncionario);
        }
        double calculaMedia = somaSalarios / totalFuncionarios; //Calcula média dos salários
        System.out.println("A média dos salários pagos é de " + calculaMedia + " eur");
    }

}
