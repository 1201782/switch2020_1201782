package bloco4;

public class Exercicio19 {
    public static String organizarNumeros(int numero){
        String pares = "";
        String impares ="";

       while (numero>0){
            int digito= numero%10;
            if (digito%2==0){
                pares = pares.concat(String.valueOf(digito));
            }else{
                impares = impares.concat(String.valueOf(digito));
            }
            numero = numero/10;
        }

        String numUnico = pares.concat(impares);
        return numUnico;
    }
}
