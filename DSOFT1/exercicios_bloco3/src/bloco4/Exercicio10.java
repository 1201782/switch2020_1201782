package bloco4;

import java.util.Scanner;

public class Exercicio10 {
    public static void main(String[] args) {
        somaNumeros();
    }

    public static void somaNumeros() {
        Scanner read = new Scanner(System.in);
        System.out.println("Introduza um número positivo");
        int numeroDado = read.nextInt(); //Duvida: como validar um numero inteiro usando regex?
        int maior = 0;
        int inteiroAnterior=1;
        for (int i = 1; i < 10000; i++) {
            inteiroAnterior *= i;
            maior=i;
            if(inteiroAnterior>numeroDado){
                i=10000;
            }
        }
        System.out.println("O maior número é " + maior);
    }
}
