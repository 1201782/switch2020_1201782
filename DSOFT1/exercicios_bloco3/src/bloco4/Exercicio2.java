package bloco4;

import java.lang.reflect.Array;
import java.util.Scanner;

public class Exercicio2 {
    public static void main(String[] args) {
        lerNotas();
    }

    public static void lerNotas() {
        //ler numero de alunos (n)
        Scanner read = new Scanner(System.in);
        System.out.println("Qual o número total de alunos?");
        double n = read.nextInt();

        int contadorPositivo=0; //contador de notas positivas
        int contadorNegativo=0; //contador de notas negativas
        double somaNegativas = 0;
        double somaPositivas = 0;

        //Ler soma das notas
        for(int i = 0; i<n;i++){
            System.out.println("Digite a nota do aluno " + (i+1));
            double nota = read.nextDouble();
            if(nota<=7){
                contadorNegativo++;
                somaNegativas = somaNegativas + nota;
            }else{
                contadorPositivo++;
                somaPositivas = somaPositivas + nota;
            }
        }
        //calcular percentagem de notas positivas
        double percenagemPositivas = (contadorPositivo*100)/n;
        System.out.println("A percentagem de notas positivas é de "+ percenagemPositivas + "%");

        //média de notas negativas
        double mediasNegativas = somaNegativas/contadorNegativo;
        System.out.println("A médias das notas negativas é de "+ mediasNegativas);
    }

}
