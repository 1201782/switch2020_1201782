package bloco4;

public class Exercicio11 {

    //apresentar todas as maneiras de obter n

    public static int totalFormasDeSomar(int n) {
        int contador = 0;
        for (int x = 0; x < 11; x++) {
            for (int y = 0; y < 11; y++) {
                if (x + y == n) {
                    contador++;
                }
            }
        }
        if(contador%2==0){
            return contador/2;
        }else{
            return (contador+1)/2;
        }
    }
}
