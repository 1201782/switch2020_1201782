package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio20Test {

    @Test
    public void categorizarNumeros_Test1() {
        String expected = "A categoria é Abundante";
        String result = Exercicio20.categorizarNumeros(12);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void categorizarNumeros_Test2() {
        String expected = "A categoria é Reduzido";
        String result = Exercicio20.categorizarNumeros(9);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void categorizarNumeros_Test3() {
        String expected = "A categoria é Perfeito";
        String result = Exercicio20.categorizarNumeros(6);
        Assert.assertEquals(expected,result);
    }
}