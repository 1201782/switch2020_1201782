package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio6Test {

    @Test
    public void numeroDeAlgarismos_Test1() {
        int expected = 8;
        int result = Exercicio6.numeroDeAlgarismos(81009653);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void numeroDeAlgarismos_Test2() {
        int expected =10;
        int result = Exercicio6.numeroDeAlgarismos(1366985124);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void numeroDeAlgarismos_Test3() {
        int expected =9;
        int result = Exercicio6.numeroDeAlgarismos(360000020);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void algarismosPares_Test1() {
        int expected =8;
        int result = Exercicio6.algarismosPares(360000020);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void algarismosPares_Test2() {
        int expected =5;
        int result = Exercicio6.algarismosPares(1366985124);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void algarismosPares_Test3() {
        int expected =5;
        int result = Exercicio6.algarismosPares(1000003);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void algarismosPares_Test4() {
        int expected =9;
        int result = Exercicio6.algarismosPares(1002244860);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void algarismosImpares_Test1() {
        int expected =1;
        int result = Exercicio6.algarismosImpares(1002244860);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void algarismosImpares_Test2() {
        int expected =2;
        int result = Exercicio6.algarismosImpares(1000003);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void algarismosImpares_Test3() {
        int expected =5;
        int result = Exercicio6.algarismosImpares(1366985124);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void somaAlgarismos_Test1() {
        int expected =21;
        int result = Exercicio6.somaAlgarismos(123456);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void somaAlgarismos_Test2() {
        int expected =4;
        int result = Exercicio6.somaAlgarismos(1000003);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void somaAlgarismos_Test3() {
        int expected =45;
        int result = Exercicio6.somaAlgarismos(1366985124);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void somaAlgarismosPares_Test1() {
        int expected =26;
        int result = Exercicio6.somaAlgarismosPares(1366985124);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void somaAlgarismosPares_Test2() {
        int expected =12;
        int result = Exercicio6.somaAlgarismosPares(123456);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void somaAlgarismosPares_Test3() {
        int expected =10;
        int result = Exercicio6.somaAlgarismosPares(15987032);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void somaAlgarismosImpares_Test1() {
        int expected =25;
        int result = Exercicio6.somaAlgarismosImpares(15987032);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void somaAlgarismosImpares_Test2() {
        int expected =39;
        int result = Exercicio6.somaAlgarismosImpares(1598790635);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void somaAlgarismosImpares_Test3() {
        int expected =20;
        int result = Exercicio6.somaAlgarismosImpares(10000937);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaAlgarismos_Test1() {
        int expected =3;
        int result = Exercicio6.mediaAlgarismos(26843026);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void mediaAlgarismos_Test2() {
        int expected =3;
        int result = Exercicio6.mediaAlgarismos(1597833210);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void mediaAlgarismos_Test3() {
        int expected =1;
        int result = Exercicio6.mediaAlgarismos(400000062);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaAlgarismosPares_Test1() {
        int expected =4;
        int result = Exercicio6.mediaAlgarismosPares(156048932);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void mediaAlgarismosPares_Test2() {
        int expected =5;
        int result = Exercicio6.mediaAlgarismosPares(56874239);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void mediaAlgarismosPares_Test3() {
        int expected =5;
        int result = Exercicio6.mediaAlgarismosPares(489632571);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaAlgarismosImpares() {
        int expected =4;
        int result = Exercicio6.mediaAlgarismosImpares(865471023);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void ordemInversa_Test1() {
        String expected ="654321";
        String result = Exercicio6.ordemInversa(123456);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void ordemInversa_Test2() {
        String expected ="329876";
        String result = Exercicio6.ordemInversa(678923);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void ordemInversa_Test3() {
        String expected ="0957842";
        String result = Exercicio6.ordemInversa(2487590);
        Assert.assertEquals(expected, result);
    }
}