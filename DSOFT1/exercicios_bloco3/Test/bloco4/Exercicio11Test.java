package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio11Test {

    @Test
    public void totalFormasDeSomar_Test1() {
        int expected= 2;
        int result = Exercicio11.totalFormasDeSomar(3);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void totalFormasDeSomar_Test2() {
        int expected= 3;
        int result = Exercicio11.totalFormasDeSomar(5);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void totalFormasDeSomar_Test3() {
        int expected= 2;
        int result = Exercicio11.totalFormasDeSomar(4);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void totalFormasDeSomar_Test4() {
        int expected= 6;
        int result = Exercicio11.totalFormasDeSomar(10);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void totalFormasDeSomar_Test5() {
        int expected= 2;
        int result = Exercicio11.totalFormasDeSomar(18);
        Assert.assertEquals(expected,result);
    }

}