package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio4Test {

    @Test
    public void multiplosDeTres_Test1() {
        int expected = 1;
        int result = Exercicio4.multiplosDeTres(3);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void multiplosDeTres_Test2() {
        int expected = 2;
        int result = Exercicio4.multiplosDeTres(6);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void multiplosDeTres_Test3() {
        int expected = 7;
        int result = Exercicio4.multiplosDeTres(21);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void multiplosDoisNumeros_Test1() {
        int expected = 5;
        int result = Exercicio4.multiplosDoisNumeros(10,2);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void multiplosDoisNumeros_Test2() {
        int expected = 5;
        int result = Exercicio4.multiplosDoisNumeros(20,4);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void multiplosDoisNumeros_Test3() {
        int expected = 7;
        int result = Exercicio4.multiplosDoisNumeros(49,7);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void multiplosDeTresECinco_Test1() {
        int expected = 1;
        int result = Exercicio4.multiplosDeTresECinco(20);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void multiplosDeTresECinco_Test2() {
        int expected = 6;
        int result = Exercicio4.multiplosDeTresECinco(100);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void multiplosDeTresECinco_Test3() {
        int expected = 13;
        int result = Exercicio4.multiplosDeTresECinco(200);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void multiplosDoisNumerosAleatorios_Test1() {
        int expected = 1;
        int result = Exercicio4.multiplosDoisNumerosAleatorios(20,2,9);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void multiplosDoisNumerosAleatorios_Test2() {
        int expected = 0;
        int result = Exercicio4.multiplosDoisNumerosAleatorios(40,8,7);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void multiplosDoisNumerosAleatorios_Test3() {
        int expected = 2;
        int result = Exercicio4.multiplosDoisNumerosAleatorios(40,2,20);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void somaMultiplos_Test1() {
        int expected = 60;
        int result = Exercicio4.somaMultiplos(20,2,4);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void somaMultiplos_Tes2() {
        int expected = 18;
        int result = Exercicio4.somaMultiplos(10,1,3);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void somaMultiplos_Test3() {
        int expected = 300;
        int result = Exercicio4.somaMultiplos(100,20,2);
        Assert.assertEquals(expected,result);
    }
}