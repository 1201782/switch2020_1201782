package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio19Test {

    @Test
    public void organizarNumeros_Test1() {
        String expected = "4259";
        String result = Exercicio19.organizarNumeros(9524);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void organizarNumeros_Test2() {
        String expected = "403571";
        String result = Exercicio19.organizarNumeros(107534);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void organizarNumeros_Test3() {
        String expected = "086933";
        String result = Exercicio19.organizarNumeros(363980);
        Assert.assertEquals(expected,result);
    }
}