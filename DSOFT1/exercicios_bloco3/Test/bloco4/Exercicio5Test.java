package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio5Test {

    @Test
    public void numerosPares_Test1() {
        int expected = 110;
        int result = Exercicio5.numerosPares(20);
        Assert.assertEquals(expected, result);

    }
    @Test
    public void numerosPares_Test2() {
        int expected = 30;
        int result = Exercicio5.numerosPares(10);
        Assert.assertEquals(expected, result);

    }
    @Test
    public void numerosPares_Test3() {
        int expected = 90;
        int result = Exercicio5.numerosPares(19);
        Assert.assertEquals(expected, result);

    }

    @Test
    public void quantidadePares_Test1() {
        int expected = 6;
        int result = Exercicio5.quantidadePares(10);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void quantidadePares_Test2() {
        int expected = 11;
        int result = Exercicio5.quantidadePares(20);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void quantidadePares_Test3() {
        int expected = 7;
        int result = Exercicio5.quantidadePares(12);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void somaNumerosImpares_Test1() {
        int expected = 25;
        int result = Exercicio5.somaNumerosImpares(10);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void somaNumerosImpares_Test2() {
        int expected = 64;
        int result = Exercicio5.somaNumerosImpares(15);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void somaNumerosImpares_Test3() {
        int expected = 9;
        int result = Exercicio5.somaNumerosImpares(6);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void quantidadeImpares_Test1() {
        int expected = 10;
        int result = Exercicio5.quantidadeImpares(20);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void quantidadeImpares_Test2() {
        int expected = 5;
        int result = Exercicio5.quantidadeImpares(10);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void quantidadeImpares_Test3() {
        int expected = 1;
        int result = Exercicio5.quantidadeImpares(1);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void somaMultiplos_Test1() {
        int expected =12;
        int result = Exercicio5.somaMultiplos(10,4,2);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void somaMultiplos_Test2() {
        int expected =30;
        int result = Exercicio5.somaMultiplos(15,1,5);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void somaMultiplos_Test3() {
        int expected =0;
        int result = Exercicio5.somaMultiplos(25,10,3);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaMultiplos_Test1() {
        double expected = 12.0;
        double result = Exercicio5.mediaMultiplos(20,2,4);
        Assert.assertEquals(expected, result,0.001);
    }
    @Test
    public void mediaMultiplos_Test2() {
        double expected = 9;
        double result = Exercicio5.mediaMultiplos(10,3,9);
        Assert.assertEquals(expected, result,0.001);
    }
    @Test
    public void mediaMultiplos_Test3() {
        double expected = 15;
        double result = Exercicio5.mediaMultiplos(15,3,5);
        Assert.assertEquals(expected, result,0.001);
    }

   @Test
    public void produtoDosMultiplos_Test1() {
        int expected = 12;
        int result = Exercicio5.produtoDosMultiplos(5,2);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void produtoDosMultiplos_Test2() {
        int expected = 25;
        int result = Exercicio5.produtoDosMultiplos(10,5);
        Assert.assertEquals(expected, result);
    }
    @Test
    public void produtoDosMultiplos_Test3() {
        int expected = 49;
        int result = Exercicio5.produtoDosMultiplos(14,7);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaMultiplosH_Test1() {
        int expected = 3;
        int result = Exercicio5.mediaMultiplosH(5,2,3);
        Assert.assertEquals(expected, result,0.001);
    }
    @Test
    public void mediaMultiplosH_Test2() {
        int expected = 8;
        int result = Exercicio5.mediaMultiplosH(10,5,9);
        Assert.assertEquals(expected, result,0.001);
    }
    @Test
    public void mediaMultiplosH_Test3() {
        int expected = 12;
        int result = Exercicio5.mediaMultiplosH(20,8,6);
        Assert.assertEquals(expected, result,0.001);
    }
}