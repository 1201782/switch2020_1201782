package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio1Test {

    @Test
    public void cicloFor1() {
        int expected = 120;
        int result = Exercicio1.cicloFor1(5);
        Assert.assertEquals(expected,result);
    }
}