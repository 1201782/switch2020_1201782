package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio7Test {

    @Test
    public void eCapicua_Test1() {
       boolean result = Exercicio7.eCapicua(123321);
        Assert.assertTrue(result);
    }
    @Test
    public void eCapicua_Test2() {
        boolean result = Exercicio7.eCapicua(45698720);
        Assert.assertFalse(result);
    }
    @Test
    public void eCapicua_Test3() {
        boolean result = Exercicio7.eCapicua(1233321);
        Assert.assertTrue(result);
    }
    @Test
    public void eCapicua_Test4() {
        boolean result = Exercicio7.eCapicua(1233431);
        Assert.assertFalse(result);
    }

    @Test
    public void eArmstrong_Test1() {
        boolean result = Exercicio7.eArmstrong(153);
        Assert.assertTrue(result);
    }
    @Test
    public void eArmstrong_Test2() {
        boolean result = Exercicio7.eArmstrong(371);
        Assert.assertTrue(result);
    }
    @Test
    public void eArmstrong_Test3() {
        boolean result = Exercicio7.eArmstrong(407);
        Assert.assertTrue(result);
    }
    @Test
    public void eArmstrong_Test4() {
        boolean result = Exercicio7.eArmstrong(111);
        Assert.assertFalse(result);
    }
    @Test
    public void eArmstrong_Test5() {
        boolean result = Exercicio7.eArmstrong(500);
        Assert.assertFalse(result);
    }

    @Test
    public void primeiraCapicua_Test1() {
        int expected = 11;
        int result = Exercicio7.primeiraCapicua(10,50);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void primeiraCapicua_Test2() {
        int expected = 22;
        int result = Exercicio7.primeiraCapicua(15,50);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void primeiraCapicua_Test3() {
        int expected = 33;
        int result = Exercicio7.primeiraCapicua(25,50);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void ultimaCapicua_Test1() {
        int expected = 44;
        int result = Exercicio7.ultimaCapicua(25,50);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void ultimaCapicua_Test2() {
        int expected = 339933;
        int result = Exercicio7.ultimaCapicua(25,339933);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void ultimaCapicua_Test3() {
        int expected =1221;
        int result = Exercicio7.ultimaCapicua(25,1221);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void quantidadeCapicuas_Test1() {
        int expected =4;
        int result = Exercicio7.quantidadeCapicuas(10,50);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void quantidadeCapicuas_Test2() {
        int expected =5;
        int result = Exercicio7.quantidadeCapicuas(50,100);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void quantidadeCapicuas_Test3() {
        int expected =5;
        int result = Exercicio7.quantidadeCapicuas(100,150);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void primeiroArmstrong_Test1() {
        int expected = 0;
        int result = Exercicio7.primeiroArmstrong(0,10);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void primeiroArmstrong_Test2() {
        int expected = 153;
        int result = Exercicio7.primeiroArmstrong(10,200);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void primeiroArmstrong_Test3() {
        int expected = 370;
        int result = Exercicio7.primeiroArmstrong(200,407);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void quantidadeDeArmstrongs_Test1() {
        int expected = 10;
        int result = Exercicio7.quantidadeDeArmstrongs(0,10);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void quantidadeDeArmstrongs_Test2() {
        int expected = 4;
        int result = Exercicio7.quantidadeDeArmstrongs(7,153);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void quantidadeDeArmstrongs_Test3() {
        int expected = 7;
        int result = Exercicio7.quantidadeDeArmstrongs(407,93084);
        Assert.assertEquals(expected,result);
    }
}