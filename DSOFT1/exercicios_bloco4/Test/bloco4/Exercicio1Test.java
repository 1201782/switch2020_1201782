package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio1Test {

    @Test
    public void descobreNumeroDeDigitosInt_Test1() {
        int expected = 5;
        int result = Exercicio1.descobreNumeroDeDigitosInt(12359);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void descobreNumeroDeDigitosInt_Test2() {
        int expected = 1;
        int result = Exercicio1.descobreNumeroDeDigitosInt(0);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void descobreNumeroDeDigitosInt_Test3() {
        int expected = 2;
        int result = Exercicio1.descobreNumeroDeDigitosInt(-12);
        Assert.assertEquals(expected,result);
    }
}