package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio4Test {

    @Test
    public void separarPares_Test1() {
        int[] expected = {2, 4, 6};
        int[] result = Exercicio4.separarPares(new int[]{5, 2, 4, 6, 9, 3});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void separarPares_Test2() {
        int[] expected = {2,4,8};
        int[] result = Exercicio4.separarPares(new int[]{2,4,8});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void separarPares_Test3() {
        int[] expected = {};
        int[] result = Exercicio4.separarPares(new int[]{1, 3, 5, 9});
        Assert.assertArrayEquals(expected, result);
    }
    @Test
    public void separarPares_Test4() {
        int[] expected = {};
        int[] result = Exercicio4.separarPares(new int[]{});
        Assert.assertArrayEquals(expected, result);
    }
    @Test
    public void separarPares_Test5() {
        int[] expected = {-2,-6};
        int[] result = Exercicio4.separarPares(new int[]{-2,-6});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void separarImpares_Test1() {
        int[] expected = {3, 3};
        int[] result = Exercicio4.separarImpares(new int[]{3, 3, 6});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void separarImpares_Test2() {
        int[] expected = {1, 1, 5, 9};
        int[] result = Exercicio4.separarImpares(new int[]{1, 1, 5, 9});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void separarImpares_Test3() {
        int[] expected = {};
        int[] result = Exercicio4.separarImpares(new int[]{6, 8, 0, 2});
        Assert.assertArrayEquals(expected, result);
    }
    @Test
    public void separarImpares_Test4() {
        int[] expected = {-1,-3};
        int[] result = Exercicio4.separarImpares(new int[]{6, -1, 0, -3});
        Assert.assertArrayEquals(expected, result);
    }
    @Test
    public void separarImpares_Test5() {
        int[] expected = {};
        int[] result = Exercicio4.separarImpares(new int[]{});
        Assert.assertArrayEquals(expected, result);
    }
}