package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio3Test {

    @Test
    public void somaVetor_Test1() {
        int expected = 25;
        int result = Exercicio3.somaVetor(new int[]{3, 6, 7, 8, 1});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void somaVetor_Test2() {
        int expected = 0;
        int result = Exercicio3.somaVetor(new int[]{});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void somaVetor_Test3() {
        int expected = 0;
        int result = Exercicio3.somaVetor(new int[]{0});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void somaVetor_Test4() {
        int expected = -11;
        int result = Exercicio3.somaVetor(new int[]{-5, 3, 6, -15});
        Assert.assertEquals(expected, result);
    }
}