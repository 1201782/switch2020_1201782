package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio17Test {
    @Test
    public void produtoMatrizPorConst_Test1() {
        int[][] expected = new int[][]{{10, 20, 30}, {20, 50, 20}};
        int[][] result = Exercicio17.produtoMatrizPorConst(new int[][]{{1, 2, 3}, {2, 5, 2}}, 10);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void produtoMatrizPorConst_Test2() {
        int[][] expected = new int[][]{{0, 0, 0}, {0, 0, 0}};
        int[][] result = Exercicio17.produtoMatrizPorConst(new int[][]{{1, 2, 3}, {2, 5, 2}}, 0);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void produtoMatrizPorConst_Test3() {
        int[][] expected = new int[][]{{1, 2, 3}, {2, 5, 2}};
        int[][] result = Exercicio17.produtoMatrizPorConst(new int[][]{{1, 2, 3}, {2, 5, 2}}, 1);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void produtoMatrizPorConst_Test4() {
        int[][] expected = null;
        int[][] result = Exercicio17.produtoMatrizPorConst(new int[][]{{1, 2}, {1, 2, 3}, {4, 9, 6, 5}}, 10);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void somaMatrizes_Test1() {
        int[][] expected = null;
        int[][] result = Exercicio17.somaMatrizes(new int[][]{{1, 2}, {1, 2}}, new int[][]{{2, 3}, {3, 4}, {2, 3}, {3, 4}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void somaMatrizes_Test2() {
        int[][] expected = new int[][]{{3, 5}, {4, 6}};
        int[][] result = Exercicio17.somaMatrizes(new int[][]{{1, 2}, {1, 2}}, new int[][]{{2, 3}, {3, 4}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void somaMatrizes_Test3() {
        int[][] expected = null;
        int[][] result = Exercicio17.somaMatrizes(new int[][]{{0}}, new int[][]{{1, 2, 3}, {2, 2}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void somaMatrizes_Test4() {
        int[][] expected = new int[][]{{1, 5, 7}, {6, 8, 6}};
        int[][] result = Exercicio17.somaMatrizes(new int[][]{{0, 3, 4}, {4, 6, 5}}, new int[][]{{1, 2, 3}, {2, 2, 1}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void produtoDeMatrizes_Test1() {
        int [][]expected = new int[][]{{0,6},{2,2}};
        int [][]result = Exercicio17.produtoDeMatrizes(new int[][]{{1,2},{1,2}}, new int[][]{{0,3},{2,1}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void produtoDeMatrizes_Test2() {
        int [][]expected = new int[][]{{1,4,9},{1,4,9}};
        int[][] result = Exercicio17.produtoDeMatrizes(new int[][]{{1,2,3},{1,2,3}}, new int[][]{{1,2,3},{1,2,3}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void produtoDeMatrizes_Test3() {
        int [][]expected = null;
        int [][]result = Exercicio17.produtoDeMatrizes(new int[][]{{1, 2}, {1, 2}}, new int[][]{{1, 2}, {1, 2},{1, 2}});
        Assert.assertArrayEquals(expected, result);
    }

}