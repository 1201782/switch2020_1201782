package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio14Test {

    @Test
    public void verificaMatrizRetangular_Test1() {
        boolean result = Exercicio14.verificaMatrizRetangular(new int[][]{{1,2,3},{2,3,3},{1,2,3},{1,2,3}});
        Assert.assertTrue(result);
    }
    @Test
    public void verificaMatrizRetangular_Test2() {
        boolean result = Exercicio14.verificaMatrizRetangular(new int[][]{{1,2,3,4,5},{2,3,3,4,5},{1,2,3,5,6}});
        Assert.assertTrue(result);
    }
    @Test
    public void verificaMatrizRetangular_Test3() {
        boolean result = Exercicio14.verificaMatrizRetangular(new int[][]{{1,2,3,4,5},{2,3,3,4,5},{1,2,3,5,6},{1,2,3,5,6},{1,2,3,5,6}});
        Assert.assertFalse(result);
    }
    @Test
    public void verificaMatrizRetangular_Test4() {
        boolean result = Exercicio14.verificaMatrizRetangular(new int[][]{{1,2,3,4,5},{2,5},{1,2,5,6}});
        Assert.assertFalse(result);
    }
}