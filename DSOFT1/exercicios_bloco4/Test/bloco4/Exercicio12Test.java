package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio12Test {

    @Test
    public void numeroColunasIguais_Test1() {
        int expected = -1;
        int result = Exercicio12.matrizNumeroDeColunasIguais(new int[][]{{1,2,3},{1,2,3,4},{1,2}});
        Assert.assertEquals(expected,result);
    }
    @Test
    public void numeroColunasIguais_Test2() {
        int expected = 3;
        int result = Exercicio12.matrizNumeroDeColunasIguais(new int[][]{{1,2,3},{1,2,3},{1,2,3},{1,2,3}});
        Assert.assertEquals(expected,result);
    }
    @Test
    public void numeroColunasIguais_Test3() {
        int expected = 3;
        int result = Exercicio12.matrizNumeroDeColunasIguais(new int[][]{{1,2,3},{1,2,4},{1,2,3}});
        Assert.assertEquals(expected,result);
    }
    @Test
    public void numeroColunasIguais_Test4() {
        int expected = 0;
        int result = Exercicio12.matrizNumeroDeColunasIguais(new int[][]{{},{}});
        Assert.assertEquals(expected,result);
    }
}