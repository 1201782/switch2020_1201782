package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio13Test {

    @Test
    public void verificaRarizQuadrada_Test1() {
        boolean result = Exercicio13.verificaMatrizQuadrada(new int[][]{{3,6},{2,2}});
        Assert.assertTrue(result);
    }
    @Test
    public void verificaRarizQuadrada_Test2() {
        boolean result = Exercicio13.verificaMatrizQuadrada(new int[][]{{3,6,3},{2,2}});
        Assert.assertFalse(result);
    }
    @Test
    public void verificaRarizQuadrada_Test3() {
        boolean result = Exercicio13.verificaMatrizQuadrada(new int[][]{{3,6},{2,2,3,6,6}});
        Assert.assertFalse(result);
    }
    @Test
    public void verificaRarizQuadrada_Test4() {
        boolean result = Exercicio13.verificaMatrizQuadrada(new int[][]{{3,6,3},{3,6,3},{3,3}});
        Assert.assertFalse(result);
    }
}