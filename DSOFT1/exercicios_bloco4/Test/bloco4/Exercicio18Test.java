package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio18Test {

    @Test
    public void matrizMascaraDeUmaLetra_Test1() {
        int[][] expected = new int[][]{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 0, 1}, {0, 0, 1}};
        int[][] result = Exercicio18.matrizMascaraDeUmaLetra(
                new char[][]{   {'a', 'b', 'c'},
                                {'b', 'a', 'i'},
                                {'d', 'k', 'a'},
                                {'d', 'k', 'a'},
                                {'d', 'k', 'a'}},'a');
        Assert.assertArrayEquals(expected, result);
    }
    @Test
    public void matrizMascaraDeUmaLetra_Test2() {
        int[][] expected = new int[][]{{0, 0, 0}, {0, 0, 0}, {1, 0, 0}, {1, 0, 0}, {1, 0, 0}};
        int[][] result = Exercicio18.matrizMascaraDeUmaLetra(
                new char[][]{   {'a', 'b', 'c'},
                        {'b', 'a', 'i'},
                        {'d', 'k', 'a'},
                        {'d', 'k', 'a'},
                        {'d', 'k', 'a'}},'d');
        Assert.assertArrayEquals(expected, result);
    }

   /* @Test
    public void existePalavra() {
        boolean result = Exercicio18.existePalavra("amor");
        Assert.assertFalse(result);

    }*/
}