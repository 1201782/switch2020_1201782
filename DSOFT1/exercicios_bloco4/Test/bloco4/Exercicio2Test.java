package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio2Test {

    @Test
    public void converteParaVetor_Test1() {
        int[] expected = {4,2,3};
        int[] result = Exercicio2.converterParaVetor(423);
        Assert.assertArrayEquals(expected,result);
    }
    @Test
    public void converteParaVetor_Test2() {
        int[] expected = {0};
        int[] result = Exercicio2.converterParaVetor(0);
        Assert.assertArrayEquals(expected,result);
    }
    @Test
    public void converteParaVetor_Test3() {
        int[] expected = {5,6,3};
        int[] result = Exercicio2.converterParaVetor(-563);
        Assert.assertArrayEquals(expected,result);
    }
}