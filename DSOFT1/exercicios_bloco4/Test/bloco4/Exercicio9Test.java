package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio9Test {

    @Test
    public void eCapicua_Test1() {
        boolean expected = true;
        boolean result = Exercicio9.eCapicua(123321);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void eCapicua_Test2() {
        boolean expected = false;
        boolean result = Exercicio9.eCapicua(12347321);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void eCapicua_Test3() {
        boolean expected = true;
        boolean result = Exercicio9.eCapicua(-11);
        Assert.assertEquals(expected,result);
    }
}