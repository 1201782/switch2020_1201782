package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio6Test {

    @Test
    public void vetorMenor_Test1() {
        int[] expected = {1, 2, 3};
        int[] result = Exercicio6.vetorMenor(new int[]{1, 2, 3, 4, 5}, 3);
        Assert.assertArrayEquals(expected, result);
    }
    @Test
    public void vetorMenor_Test2() {
        int[] expected = {};
        int[] result = Exercicio6.vetorMenor(new int[]{6, 3, 25, 110}, 0);
        Assert.assertArrayEquals(expected, result);
    }
    @Test
    public void vetorMenor_Test3() {
        int[] expected = {-1, -2, 3, 4, 5};
        int[] result = Exercicio6.vetorMenor(new int[]{-1, -2, 3, 4, 5, 6, 7, 8, 9, 10}, 5);
        Assert.assertArrayEquals(expected, result);
    }
    @Test
    public void vetorMenor_Test4() {
        int[] expected = null;
        int[] result = Exercicio6.vetorMenor(new int[]{-1, -2, 3, 4}, 5);
        Assert.assertArrayEquals(expected, result);
    }
    @Test
    public void vetorMenor_Test5() {
        int[] expected = null;
        int[] result = Exercicio6.vetorMenor(new int[]{-1, -2, 3, 4}, -5);
        Assert.assertArrayEquals(expected, result);
    }
}