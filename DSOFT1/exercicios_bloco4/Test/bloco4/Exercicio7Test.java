package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio7Test {

    @Test
    public void multiplosDeN_Test1() {
        int[] expected = {3, 6, 9};
        int[] result = Exercicio7.multiplosDeN(1, 10, 3);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void multiplosDeN_Test2() {
        int[] expected = {0, 2, 4, 6, 8, 10};
        int[] result = Exercicio7.multiplosDeN(0, 10, 2);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void multiplosDeN_Test3() {
        int[] expected = null;
        int[] result = Exercicio7.multiplosDeN(0, 10, 0);
        Assert.assertArrayEquals(expected, result);
    }

}