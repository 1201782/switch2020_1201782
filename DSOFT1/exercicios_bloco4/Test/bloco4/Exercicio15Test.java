package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio15Test<expected> {

    @Test
    public void menorInteiro_Test1() {
        int expected = 1;
        int result = Exercicio15.menorInteiro(new int[][]{{1, 6, 9}, {2, 3, 6}, {1, 2, 8}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void menorInteiro_Test2() {
        int expected = 1;
        int result = Exercicio15.menorInteiro(new int[][]{{1, 6, 9}, {2, 3, 6}, {1, 2, 8}, {1, 2, 9}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void menorInteiro_Test3() {
        int expected = 1;
        int result = Exercicio15.menorInteiro(new int[][]{{1, 1}, {1, 1}, {1, 1}, {1, 1}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void maiorInteiro_Test1() {
        int expected = 9;
        int result = Exercicio15.maiorInteiro(new int[][]{{1, 6, 9}, {2, 3, 6}, {1, 2, 8}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void maiorInteiro_Test2() {
        int expected = 9;
        int result = Exercicio15.maiorInteiro(new int[][]{{1, 6, 9}, {2, 3, 6}, {1, 2, 8}, {1, 2, 9}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void maiorInteiro_Test3() {
        int expected = 1;
        int result = Exercicio15.maiorInteiro(new int[][]{{1, 1}, {1, 1}, {1, 1}, {1, 1}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void valorMedioElementos_Test1() {
        int expected = 1;
        int result = Exercicio15.valorMedioElementos(new int[][]{{1, 1}, {1, 1}, {1, 1}, {1, 1}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void valorMedioElementos_Test2() {
        int expected = 3;
        int result = Exercicio15.valorMedioElementos(new int[][]{{1, 2}, {8, 4}, {4, 7, 1}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void valorMedioElementos_Test3() {
        int expected = 4;
        int result = Exercicio15.valorMedioElementos(new int[][]{{1, 3, 10}, {8, 4, 2}, {8, 4, 2}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void produtoElementos_Test1() {
        int expected = 122880;
        int result = Exercicio15.produtoElementos(new int[][]{{1, 3, 10}, {8, 4, 2}, {8, 4, 2}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void produtoElementos_Test2() {
        int expected = 0;
        int result = Exercicio15.produtoElementos(new int[][]{{1, 3, 0}, {8, 4, 2}, {8, 4, 2}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void produtoElementos_Test3() {
        int expected = 3;
        int result = Exercicio15.produtoElementos(new int[][]{{1, 1, 3}, {1}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void elementosUnicos_Test1() {
        int[] expected = new int[]{1, 2, 3, 4, 5, 6};
        int[] result = Exercicio15.elementosUnicos(new int[][]{{1, 2, 3}, {4, 5, 6}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void elementosUnicos_Test2() {
        int[] expected = new int[]{1, 2, 3, 4};
        int[] result = Exercicio15.elementosUnicos(new int[][]{{1, 2, 3}, {4, 1}, {1, 1}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void elementosUnicos_Test3() {
        int[] expected = new int[]{0, 2, 3, 4};
        int[] result = Exercicio15.elementosUnicos(new int[][]{{0, 2, 3}, {4, 0, 2}, {2, 0, 3}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void elementosUnicos_Test34() {
        int[] expected = new int[]{1, 2, 3, 4, 0};
        int[] result = Exercicio15.elementosUnicos(new int[][]{{1, 2, 3}, {4, 0, 2}, {2, 0, 3}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void diagonalPrincipal_Test1() {
        int[] expected = new int[]{1, 0, 3};
        int[] result = Exercicio15.diagonalPrincipal(new int[][]{{1, 2, 3}, {4, 0, 2}, {2, 0, 3}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void diagonalPrincipal_Test2() {
        int[] expected = null;
        int[] result = Exercicio15.diagonalPrincipal(new int[][]{{1, 2, 3}, {4, 0, 2}, {2, 3}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void diagonalPrincipal_Test3() {
        int[] expected = null;
        int[] result = Exercicio15.diagonalPrincipal(new int[][]{});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void diagonalPrincipal_Test4() {
        int[] expected = {1, 0, 3};
        int[] result = Exercicio15.diagonalPrincipal(new int[][]{{1, 2, 3}, {4, 0, 2}, {2, 3, 3}, {2, 3, 6}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void diagonalPrincipal_Test5() {
        int[] expected = {1, 0, 3, 0};
        int[] result = Exercicio15.diagonalPrincipal(new int[][]{{1, 2, 3, 2, 3}, {4, 0, 2, 3, 6}, {2, 3, 3, 3, 2}, {2, 3, 6, 0, 1}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void diagonalSecundaria_Test1() {
        int[] expected = {3, 3, 3, 3};
        int[] result = Exercicio15.diagonalSecundaria(new int[][]{{1, 2, 3, 2, 3}, {4, 0, 2, 3, 6}, {2, 3, 3, 3, 2}, {2, 3, 6, 0, 1}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void diagonalSecundaria_Test2() {
        int[] expected = null;
        int[] result = Exercicio15.diagonalSecundaria(new int[][]{});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void diagonalSecundaria_Test3() {
        int[] expected = {2, 1, 0};
        int[] result = Exercicio15.diagonalSecundaria(new int[][]{{0, 1, 2}, {0, 1, 2}, {0, 1, 2}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void diagonalSecundaria_Test4() {
        int[] expected = {2, 1, 0};
        int[] result = Exercicio15.diagonalSecundaria(new int[][]{{0, 1, 2}, {0, 1, 2}, {0, 1, 2},{0, 1, 2},{0, 1, 2}});
        Assert.assertArrayEquals(expected, result);
    }
}