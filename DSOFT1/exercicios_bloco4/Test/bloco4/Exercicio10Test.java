package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio10Test {

    @Test
    public void elementosUnicosNoVetor_Test1() {
        int[] expected = {1,2,3};
        int [] result = Exercicio10.elementosUnicosNoVetor(new int[]{1,2,3,3});
        Assert.assertArrayEquals(expected,result);
    }

    @Test
    public void elementosUnicosNoVetor_Test2() {
        int[] expected = {0,1,2,3};
        int [] result = Exercicio10.elementosUnicosNoVetor(new int[]{0,1,2,3});
        Assert.assertArrayEquals(expected,result);
    }
    @Test
    public void elementosUnicosNoVetor_Test3() {
        int[] expected = {0,1,2,3};
        int [] result = Exercicio10.elementosUnicosNoVetor(new int[]{0,1,2,3,1,2,3});
        Assert.assertArrayEquals(expected,result);
    }
    @Test
    public void elementosUnicosNoVetor_Test4() {
        int[] expected = {1,2,3,0};
        int [] result = Exercicio10.elementosUnicosNoVetor(new int[]{1,1,2,3,0,2,3,0});
        Assert.assertArrayEquals(expected,result);
    }

}