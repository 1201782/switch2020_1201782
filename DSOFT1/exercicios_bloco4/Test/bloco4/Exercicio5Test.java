package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio5Test {

    @Test
    public void somaNumerosPares_Test1() {
        int expected= 8;
        int result= Exercicio5.somaNumerosPares(34451);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void somaNumerosPares_Test2() {
        int expected= 8;
        int result= Exercicio5.somaNumerosPares(8);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void somaNumerosPares_Test3() {
        int expected= 0;
        int result= Exercicio5.somaNumerosPares(-13);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void somaNumerosPares_Test4() {
        int expected= 4;
        int result= Exercicio5.somaNumerosPares(-1223);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void somaNumerosImpares_Test1() {
        int expected= 9;
        int result= Exercicio5.somaNumerosImpares(34458621);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void somaNumerosImpares_Test2() {
        int expected= 10;
        int result= Exercicio5.somaNumerosImpares(-19);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void somaNumerosImpares_Test3() {
        int expected= 0;
        int result= Exercicio5.somaNumerosImpares(2248);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void somaNumerosImpares_Test4() {
        int expected= 9;
        int result= Exercicio5.somaNumerosImpares(135);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void somaNumerosImpares_Test5() {
        int expected= 0;
        int result= Exercicio5.somaNumerosImpares(0);
        Assert.assertEquals(expected,result);
    }
}