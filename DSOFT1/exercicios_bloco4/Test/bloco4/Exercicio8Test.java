package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio8Test {

    @Test
    public void multiplosComuns_Test1() {
        int[] expected = {0};
        int [] result = Exercicio8.multiplosComuns(0,4, new int[]{2,3});
        Assert.assertArrayEquals(expected,result);
    }
    @Test
    public void multiplosComuns_Test2() {
        int[] expected = {6, 12};
        int [] result = Exercicio8.multiplosComuns(4,12, new int[]{2, 3});
        Assert.assertArrayEquals(expected,result);
    }
    @Test
    public void multiplosComuns_Test3() {
        int[] expected = null;
        int [] result = Exercicio8.multiplosComuns(4,12, new int[]{});
        Assert.assertArrayEquals(expected,result);
    }
    @Test
    public void multiplosComuns_Test4() {
        int[] expected = {4,5,6,7,8,9,10,11,12};
        int [] result = Exercicio8.multiplosComuns(4,12, new int[]{1});
        Assert.assertArrayEquals(expected,result);
    }
}