package bloco4;

public class Exercicio17 {
    /**
     * Descrição: produto de uma matriz por uma constante
     *
     * @param matriz
     * @param numero
     * @return
     */
    public static int[][] produtoMatrizPorConst(int[][] matriz, int numero) {
        // garanto que é regular:
        boolean temMesmoNumeroColunas = Exercicio12.matrizDeColunasIguais(matriz);
        int[][] produtoMatriz = new int[matriz.length][matriz[0].length];
        if (temMesmoNumeroColunas) {
            for (int linha = 0; linha < matriz.length; linha++) {
                for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                    produtoMatriz[linha][coluna] = (matriz[linha][coluna]) * numero;
                }
            }
        } else {
            produtoMatriz = null;
        }
        return produtoMatriz;
    }


    /**
     * Descrição1: soma de duas matrizes
     *
     * @param matrizA
     * @param matrizB
     * @return
     */
    public static int[][] somaMatrizes(int[][] matrizA, int[][] matrizB) {
        boolean matrizAERegular = Exercicio12.matrizDeColunasIguais(matrizA);
        boolean matrizBERegular = Exercicio12.matrizDeColunasIguais(matrizB);
        int [][]matrizSoma = new int[matrizA.length][matrizA[0].length];
        if (matrizAERegular && matrizBERegular) {
            if (matrizA[0].length == matrizB[0].length) {
                if(matrizA.length == matrizB.length){
                    for (int linha = 0; linha < matrizA.length; linha++) {
                        for (int coluna = 0; coluna < matrizA[linha].length; coluna++) {
                            matrizSoma[linha][coluna]= matrizA[linha][coluna] + matrizB[linha][coluna];
                        }
                    } 
                }else{
                    matrizSoma =null;
                }
            }else{
                matrizSoma =null;
            }
        }else{
            matrizSoma =null;
        }
        return matrizSoma;
    }

    /**
     * Descrição: produto de duas matrizes
     *
     * @param matrizA
     * @param matrizB
     * @return
     */
    public static int [][] produtoDeMatrizes(int[][] matrizA, int[][] matrizB) {
        boolean matrizAERegular = Exercicio12.matrizDeColunasIguais(matrizA);
        boolean matrizBERegular = Exercicio12.matrizDeColunasIguais(matrizB);
        int [][]matrizProduto = new int[matrizA.length][matrizA[0].length];
        if (matrizAERegular && matrizBERegular) {
            if (matrizA[0].length == matrizB[0].length) {
                if(matrizA.length == matrizB.length){
                    for (int linha = 0; linha < matrizA.length; linha++) {
                        for (int coluna = 0; coluna < matrizA[linha].length; coluna++) {
                            matrizProduto[linha][coluna]= matrizA[linha][coluna] * matrizB[linha][coluna];
                        }
                    }
                }else{
                    matrizProduto =null;
                }
            }else{
                matrizProduto =null;
            }
        }else{
            matrizProduto =null;
        }
        return matrizProduto;

    }
}
