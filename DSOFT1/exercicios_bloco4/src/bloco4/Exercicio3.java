package bloco4;

public class Exercicio3 {
    /**
     * Descrição: faz a soma dos elementos de um vetor inteiro
     * 
     * @param inteiros
     * @return
     */
    public static int somaVetor(int [] inteiros){
        int somaVetores =0;
        for (int inteiro = 0; inteiro <= inteiros.length -1; inteiro++) {
            somaVetores += inteiros[inteiro];
        }
          return somaVetores;
    }
}
