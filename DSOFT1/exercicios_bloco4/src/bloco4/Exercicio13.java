package bloco4;

public class Exercicio13 {
    /**
     * Descrição: verifica se uma matriz é quadrada
     *
     * @param matriz
     * @return
     */
    public static boolean verificaMatrizQuadrada(int[][] matriz) {
        boolean matrizQuadrada = false;
        int quantColunas;
        int quantLinhas = matriz.length;
        for (int[] ints : matriz) {
            quantColunas = ints.length;
            if (quantLinhas == quantColunas) {
                matrizQuadrada = true;
            } else {
                matrizQuadrada = false;
                return matrizQuadrada;
            }
        }
        return matrizQuadrada;
    }
}
