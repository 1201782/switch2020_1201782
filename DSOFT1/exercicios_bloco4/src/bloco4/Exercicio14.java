package bloco4;

public class Exercicio14 {
    /**
     * Descrição: verifica se uma matriz é retangular
     *
     * @param matriz
     * @return
     */
    public static boolean verificaMatrizRetangular(int[][] matriz) {
        boolean matrizRetangular = false;
        boolean matrizMesmoNumeroDeColunas = Exercicio12.matrizDeColunasIguais(matriz);
        int quantLinha = matriz.length;
        int quantColuna= matriz[0].length;

        if(matrizMesmoNumeroDeColunas){
            if(quantColuna!=quantLinha){
                matrizRetangular=true;
            }
        }
        return matrizRetangular;
    }
}
