package bloco4;


public class Exercicio2 {
    /**
     * Descrição: converte um inteiro em vetor
     *
     * @param numero
     * @return
     */
    public static int[] converterParaVetor(int numero) {
        int algarismos = numero;
        if(algarismos<0){
            algarismos=-algarismos;
        }
        int quantidadeDigitos = Exercicio1.descobreNumeroDeDigitosInt(numero);

        int[] numeros = new int[quantidadeDigitos];

        for (int index = quantidadeDigitos - 1; index >= 0; index--) {
            numeros[index] = (algarismos % 10);
            if (algarismos < 10) {
                numeros[index] = algarismos;
            }
            algarismos = algarismos / 10;
        }

        return numeros;
    }
}
