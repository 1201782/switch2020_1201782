package bloco4;

public class Exercicio12 {
    /**
     * Descrição: verifica se uma matriz é quadrada e retorna um inteiro como verificação
     *
     * @param matriz
     * @return
     */
    public static int matrizNumeroDeColunasIguais(int[][]matriz){
        if(matriz.length==0){
            return 0;
        }
        int resultado = 0;
        int baseTotalColunas = matriz[0].length;
        for (int linha = 0; linha < matriz.length; linha++) {
            if(matriz[linha].length==baseTotalColunas){
                resultado = baseTotalColunas;
            }else{
                resultado=-1;
            }
        }
        return resultado;
    }
//fiz um booleano para reaproveitar
    public static boolean matrizDeColunasIguais(int[][]matriz){
        if(matriz.length==0){
            return false;
        }
        boolean resultado = false;
        int baseTotalColunas = matriz[0].length;
        for (int linha = 0; linha < matriz.length; linha++) {
            if(matriz[linha].length==baseTotalColunas){
                resultado = true;
            }else{
                resultado=false;
            }
        }
        return resultado;
    }

}
