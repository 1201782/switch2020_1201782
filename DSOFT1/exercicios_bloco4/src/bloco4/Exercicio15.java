package bloco4;

public class Exercicio15 {
    /**
     * Descrição: encontra o menor inteiro dentro de uma matriz
     * @param matriz
     * @return
     */
    public static int menorInteiro(int[][] matriz) {
        int menor = matriz[0][0];
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (matriz[linha][coluna] < menor) {
                    menor = matriz[linha][coluna];
                }
            }
        }
        return menor;
    }

    /**
     * Descrição: encontra o maior inteiro dentro de uma matriz
     * @param matriz
     * @return
     */
    public static int maiorInteiro(int[][] matriz) {
        int maior = matriz[0][0];
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (matriz[linha][coluna] > maior) {
                    maior = matriz[linha][coluna];
                }
            }
        }
        return maior;
    }

    /**
     * Descrição:calcula o valor médio dos elementos de uma matriz
     * @param matriz
     * @return
     */
    public static int valorMedioElementos(int[][] matriz) {
        int quantElementos = 0;
        int somaElementos = 0;

        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                somaElementos += matriz[linha][coluna];
                quantElementos++;
            }
        }
        int valorMedio = somaElementos / quantElementos;
        return valorMedio;
    }

    /**
     * Descrição: calcula o produto dos elementos de uma matriz
     * @param matriz
     * @return
     */
    public static int produtoElementos(int[][] matriz) {
        int produto = 1;
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                produto *= matriz[linha][coluna];
            }
        }
        return produto;
    }

    /**
     * Descrição: a partir de um array cria um vetor com os elementos únicos
     * @param matriz
     * @return
     */
    public static int[] elementosUnicos(int[][] matriz) {
        if (matriz == null) {
            return null;
        }
        int[] resultado = new int[0];
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (!existeNoVetor(resultado, matriz[linha][coluna])) {
                    resultado = adicionarNoVertor(resultado, matriz[linha][coluna]);
                }
            }
        }
        /*int[] elementosUnicos = new int[((matriz.length) * 10)];
        int index = 0;
        int ocorrencia = 0;

        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {

                if (index == 0) {
                    elementosUnicos[index] = matriz[linha][coluna];//o primeiro elemento é a referencia
                    index++;
                } else {
                    for (int percorreVetor = 0; percorreVetor < elementosUnicos.length; percorreVetor++) {
                        if (matriz[linha][coluna]==elementosUnicos[percorreVetor]) {
                            ocorrencia++;
                        }
                    }
                    if (ocorrencia == 0) {
                        elementosUnicos[index] = matriz[linha][coluna];
                        index++;
                    }
                }
                ocorrencia = 0;
            }
        }

        int[] resultado = new int[index];
        for (int novoIndex = 0; novoIndex < index; novoIndex++) {
            resultado[novoIndex] = elementosUnicos[novoIndex];
        }*/
        return resultado;
    }

    /**
     * Descrição: adiciona um número a um vetor
     * @param vetor
     * @param numero
     * @return
     */
    private static int[] adicionarNoVertor(int[] vetor, int numero) {
        int size = 1;
        if (vetor != null) {
            size += vetor.length;
        }
        int[] result = new int[size];
        for (int pos = 0; pos < size - 1; pos++) {
            result[pos] = vetor[pos];
        }
        result[size - 1] = numero;
        return result;
    }

    /**
     * Descrição: verifica se um dado número pertence ao vetor
     * @param vetor
     * @param numero
     * @return
     */
    private static boolean existeNoVetor(int[] vetor, int numero) {
        if (vetor == null) {
            return false;
        }
        boolean result = false;
        int pos = 0;
        while (pos < vetor.length && !result) {
            if (vetor[pos] == numero) {
                result = true;
            }
            pos++;
        }
        return result;
    }

    /**
     * Descrição: encontra a diagonal principal de uma matriz quadrada ou retangular
     *
     * @param matriz
     * @return
     */
    public static int[] diagonalPrincipal(int[][] matriz) {
        if (matriz.length == 0) {
            return null;
        }
        boolean ehQuadrada = Exercicio13.verificaMatrizQuadrada(matriz);
        boolean ehRetangular = Exercicio14.verificaMatrizRetangular(matriz);
        int indexVetor = 0;
        int tamanhoVetor = matriz.length;
        if (tamanhoVetor > matriz[0].length) {
            tamanhoVetor = matriz.length - 1;
        }
        int[] vetorDiagonal = new int[tamanhoVetor];
        if (ehQuadrada || ehRetangular) {
            for (int linha = 0; linha < matriz.length; linha++) {
                for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                    if (linha == coluna) {
                        vetorDiagonal[indexVetor] = matriz[linha][coluna];
                        indexVetor++;
                    }
                }
            }
        } else {
            return null;
        }
        return vetorDiagonal;
    }

    /**
     * Descrição: encontra a diagonal segundária de uma matriz quadrada ou retangular
     * @param matriz
     * @return
     */
    public static int[] diagonalSecundaria(int[][] matriz) {
        if (matriz.length == 0) {
            return null;
        }
        boolean ehQuadrada = Exercicio13.verificaMatrizQuadrada(matriz);
        boolean ehRetangular = Exercicio14.verificaMatrizRetangular(matriz);

        int tamanhoVetor;
        if(matriz.length < matriz[0].length){
            tamanhoVetor =matriz.length;
        }else if(matriz.length>matriz[0].length){
            tamanhoVetor = matriz[0].length;
        }else{
            tamanhoVetor =matriz.length;
        }
        int[] diagonalSecundaria = new int[tamanhoVetor];

        int indexVetor = 0;
        if (ehQuadrada || ehRetangular) {
            for (int linha = 0; linha < matriz.length; linha++) {
                for (int coluna = (matriz[linha].length-1); coluna >= 0; coluna--) {

                    int somaLinhaColuna = linha + coluna;
                    if (somaLinhaColuna == matriz.length) {
                        diagonalSecundaria[indexVetor] = matriz[linha][coluna];
                        indexVetor++;
                    }
                }
            }
        } else {
            return null;
        }
        return diagonalSecundaria;
    }

    //faltam: f,i,j,k
}
