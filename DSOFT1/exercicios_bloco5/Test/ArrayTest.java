import org.junit.Assert;
import org.junit.Test;

public class ArrayTest {

    @Test
    public void validaElementoNaPosicaoXDoVetorEIgual() {
        Array vetor = new Array(2, 3, 6);
        int result = 2;
        int posicao = 0;
        int expected = vetor.retornaElementoNaPosicaoXDoVetor(posicao);
        Assert.assertEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validaExcecaoNegativa() {
        Array vetor = new Array(2, 3, 6);
        int posicao = -10;
        vetor.retornaElementoNaPosicaoXDoVetor(posicao);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validaExcecaoMaiorQueTamanho() {
        Array vetor = new Array(2, 3, 6);
        int posicao = 10;
        vetor.retornaElementoNaPosicaoXDoVetor(posicao);
    }

    /*@Test
    public void validaElementoNaPosicaoXDoVetorNaoIgual() {
        Array vetor = new Array(2, 3, 6);
        int result = 8;
        int posicao=0;
        int expected = vetor.retornaElementoNaPosicaoXDoVetor(0);
        Assert.assertNotEquals(expected, result);
    }
*/
    @Test(expected = IllegalArgumentException.class)
    public void validaExcecao() {
        Array vetor = new Array(2, 3, 6);
        vetor.retornaElementoNaPosicaoXDoVetor(-1);
    }
    //========================================================================

    @Test
    public void retornaNumeroElementosVetor() {
        Array vetor = new Array();
        int expected = 3;
        int result = vetor.retornaNumeroElementosVetor(new int[]{8, 9, 6});
        Assert.assertEquals(expected, result);
    }

    @Test(expected = NullPointerException.class)//quando o vetor não foi instanciado
    public void retornaVetorNulo() {
        Array vetor = null;
        vetor.retornaNumeroElementosVetor(null);
    }
    //==========================================================================

    @Test
    public void retornaMaiorElementoVetor() {
        Array vetor = new Array(20, 15, 10);
        int expected = 20;
        int result = vetor.retornaMaiorElementoVetor(new int[]{20, 15, 10});
        Assert.assertEquals(expected, result);
    }

    @Test(expected = NullPointerException.class)
    public void retornaNulo() {
        Array vetor = null;
        int result = vetor.retornaMaiorElementoVetor(null);
    }

    @Test
    public void retornaMaiorComNegativosEPositos() {
        Array vetor = new Array(-20, 15, 10);
        int expected = 15;
        int result = vetor.retornaMaiorElementoVetor(new int[]{-20, 15, -10});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void retornaMaiorComNegativos() {
        Array vetor = new Array(-20, -15, -10);
        int expected = -10;
        int result = vetor.retornaMaiorElementoVetor(new int[]{-20, -15, -10});
        Assert.assertEquals(expected, result);
    }

    //==================================================================================

    @Test
    public void retornaMenorElementoVetor() {
        Array vetor = new Array(2, 3, 4);
        int expected = 2;
        int result = vetor.retornaMenorElementoVetor(new int[]{2,3,4});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void retornaMenorElementoVetor_Test2() {
        Array vetor = new Array();
        int expected = -4;
        int result = vetor.retornaMenorElementoVetor(new int[]{0,55,-4});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void retornaMenorElementoVetor_Test3() {
        Array vetor = new Array(0, 0, 0);
        int expected = 0;
        int result = vetor.retornaMenorElementoVetor(new int[]{0,0,0});
        Assert.assertEquals(expected, result);
    }

    //============================================================================

    @Test
    public void mediaDosElementosDoVetor() {
        Array vetor = new Array();
        int expected = 2;
        int result = vetor.mediaDosElementosDoVetor(new int[]{1,2,3});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaDosElementosDoVetor_Test2() {
        Array vetor = new Array();
        int expected = -5;
        int result = vetor.mediaDosElementosDoVetor(new int[]{-1,-5,-9});
        Assert.assertEquals(expected, result);
    }

    //=================================================================

    @Test
    public void mediaDosElementosParesDoVetor() {
        Array vetor = new Array(1, 3, 5);
        int expected = 0;
        int result = vetor.mediaDosElementosParesDoVetor();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaDosElementosParesDoVetor_Test2() {
        Array vetor = new Array(2, 6, 8);
        int expected = 5;
        int result = vetor.mediaDosElementosParesDoVetor();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaDosElementosParesDoVetor_Test3() {
        Array vetor = new Array(-2, 5, 8);
        int expected = 3;
        int result = vetor.mediaDosElementosParesDoVetor();
        Assert.assertEquals(expected, result);
    }

    //==============================================================================

    @Test
    public void mediaDosElementosImparesDoVetor() {
        Array vetor = new Array(2, 8, 6);
        int expected = 0;
        int result = vetor.mediaDosElementosImparesDoVetor();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaDosElementosImparesDoVetor_Test2() {
        Array vetor = new Array(3, 5, 9);
        int expected = 5;
        int result = vetor.mediaDosElementosImparesDoVetor();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaDosElementosImparesDoVetor_Test3() {
        Array vetor = new Array(0, 0, 0);
        int expected = 0;
        int result = vetor.mediaDosElementosImparesDoVetor();
        Assert.assertEquals(expected, result);
    }

    //==========================================================================
    @Test
    public void mediaDosMultiplosDeNumero() {
        Array vetor = new Array(2, 4, 6);
        int expected = 4;
        int result = vetor.mediaDosMultiplosDeNumero(2);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaDosMultiplosDeNumero_Test2() {
        Array vetor = new Array(3, 5, 9);
        int expected = 0;
        int result = vetor.mediaDosMultiplosDeNumero(2);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaDosMultiplosDeNumero_Test3() {
        Array vetor = new Array(2, -4, -6);
        int expected = -2;
        int result = vetor.mediaDosMultiplosDeNumero(2);
        Assert.assertEquals(expected, result);
    }

    //=============================================================================

    @Test
    public void verificaSeVetorEVazio() {
        Array vetor = new Array(2, -4, -6);
        boolean result = vetor.verificaSeVetorEVazio();
        Assert.assertFalse(result);
    }

    @Test
    public void verificaSeVetorEVazio_Test2() {
        Array vetor = new Array();
        boolean result = vetor.verificaSeVetorEVazio();
        Assert.assertTrue(result);
    }

    @Test
    public void verificaSeVetorEVazio_Test3() {
        Array vetor = new Array();
        vetor.adicionarElemento(6);
        boolean result = vetor.verificaSeVetorEVazio();
        Assert.assertFalse(result);
    }

    //===============================================================================
    @Test
    public void verificaSeTemApenasUmElemento() {
        Array vetor = new Array(2, -4, -6);
        boolean result = vetor.verificaSeTemApenasUmElemento();
        Assert.assertFalse(result);
    }

    @Test
    public void verificaSeTemApenasUmElemento_Test2() {
        Array vetor = new Array();
        boolean result = vetor.verificaSeTemApenasUmElemento();
        Assert.assertFalse(result);
    }

    @Test
    public void verificaSeTemApenasUmElemento_Test3() {
        Array vetor = new Array();
        vetor.adicionarElemento(2);
        boolean result = vetor.verificaSeTemApenasUmElemento();
        Assert.assertTrue(result);
    }

    @Test
    public void verificaSeTemApenasUmElemento_Test4() {
        Array vetor = new Array();
        vetor.adicionarElemento(2);
        vetor.adicionarElemento(4);
        boolean result = vetor.verificaSeTemApenasUmElemento();
        Assert.assertFalse(result);
    }

    //=========================================================================
    @Test
    public void verificaSeVetorSotemPares() {
        Array vetor = new Array(22, -4, -6);
        boolean result = vetor.verificaSeVetorSotemPares();
        Assert.assertTrue(result);
    }

    @Test
    public void verificaSeVetorSotemPares_Test2() {
        Array vetor = new Array(1, 3, 5);
        boolean result = vetor.verificaSeVetorSotemPares();
        Assert.assertFalse(result);
    }

    @Test
    public void verificaSeVetorSotemPares_Test3() {
        Array vetor = new Array(-2, -6, -8);
        boolean result = vetor.verificaSeVetorSotemPares();
        Assert.assertTrue(result);
    }

    //=============================================================================
    @Test
    public void verificaSeVetorSotemImpares() {
        Array vetor = new Array(-2, -6, -8);
        boolean result = vetor.verificaSeVetorSotemImpares();
        Assert.assertFalse(result);
    }

    @Test
    public void verificaSeVetorSotemImpares_Test2() {
        Array vetor = new Array(1, 3, 7);
        boolean result = vetor.verificaSeVetorSotemImpares();
        Assert.assertTrue(result);
    }

    @Test
    public void verificaSeVetorSotemImpares_Test3() {
        Array vetor = new Array(1, 2, 7);
        boolean result = vetor.verificaSeVetorSotemImpares();
        Assert.assertFalse(result);
    }

    @Test
    public void verificaSeVetorSotemImpares_Test4() {
        Array vetor = new Array(2, 6, 8);
        boolean result = vetor.verificaSeVetorSotemImpares();
        Assert.assertFalse(result);
    }

    //============================================================================
    @Test
    public void verificaElementosRepetidos() {
        Array vetor = new Array(2, 6, 8);
        boolean result = vetor.verificaElementosRepetidos();
        Assert.assertFalse(result);
    }

    @Test
    public void verificaElementosRepetidos_Test2() {
        Array vetor = new Array(2, 2, 3);
        boolean result = vetor.verificaElementosRepetidos();
        Assert.assertTrue(result);
    }

    @Test
    public void verificaElementosRepetidos_Test3() {
        Array vetor = new Array(2, 2, 2);
        boolean result = vetor.verificaElementosRepetidos();
        Assert.assertTrue(result);
    }

    @Test
    public void verificaElementosRepetidos_Test4() {
        Array vetor = new Array(2, 2, 3);
        vetor.retiraPrimeiroElemento(new int[]{2,2,3});
        boolean result = vetor.verificaElementosRepetidos();
        Assert.assertFalse(result);
    }

    //===============================================================================================

    @Test
    public void mediaAlgarismosDoVetor() {
        Array vetor = new Array();
        int expected = 3;
        int result = vetor.mediaAlgarismosDoVetor(new int[]{225, -45, 9256});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaAlgarismosDoVetor_Test2() {
        Array vetor = new Array();
        int expected = 1;
        int result = vetor.mediaAlgarismosDoVetor(new int[]{1, 2, 6});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaAlgarismosDoVetor_Test3() {
        Array vetor = new Array();
        int expected = 0;
        int result = vetor.mediaAlgarismosDoVetor(new int[0]);
        Assert.assertEquals(expected, result);
    }

    //=================================================================================

    @Test
    public void totalAlgarismosVetor() {
        Array vetor = new Array();
        int expected = 6;
        int result = vetor.totalAlgarismosVetor(new int[]{15, 60, 88});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void totalAlgarismosVetor_Test2() {
        Array vetor = new Array();
        int expected = 9;
        int result = vetor.totalAlgarismosVetor(new int[]{15, 0, 855555});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void totalAlgarismosVetor_Test3() {
        Array vetor = new Array();
        int expected = 0;
        int result = vetor.totalAlgarismosVetor(new int[0]);
        Assert.assertEquals(expected, result);
    }

    //=============================================================================================
    @Test
    public void numeroDeDigitosInt() {
        Array vetor = new Array();
        int expected = 4;
        int result = vetor.numeroDeDigitosInt(5894);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void numeroDeDigitosInt_Test2() {
        Array vetor = new Array();
        int expected = 1;
        int result = vetor.numeroDeDigitosInt(0);
        Assert.assertEquals(expected, result);
    }

    //===================================================================================================

    @Test
    public void algarismosAcimaDaMedia() {
        Array vetor = new Array();
        int[] expected = {817};
        int[] result = vetor.algarismosAcimaDaMedia(new int[]{56, 90, 817});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void algarismosAcimaDaMedia_Test2() { // não era suposto ser vazio?
        Array vetor = new Array();
        int[] expected = {22, 99};
        int[] result = vetor.algarismosAcimaDaMedia(new int[]{1, 22, 99});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void algarismosAcimaDaMedia_Test3() {
        Array vetor = new Array();
        int[] expected = {};
        int[] result = vetor.algarismosAcimaDaMedia(new int[]{1, 2, 3});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void algarismosAcimaDaMedia_Test4() {
        Array vetor = new Array();
        int[] expected = {10};
        int[] result = vetor.algarismosAcimaDaMedia(new int[]{10, 2, 3});
        Assert.assertArrayEquals(expected, result);
    }

    //===========================================================================================
    @Test
    public void percentagemAlgarismosParesVetor() {
        Array vetor = new Array();
        int expected = 33;
        int result = vetor.percentagemAlgarismosParesVetor(new int[]{1, 2, 3});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void percentagemAlgarismosParesVetor_Test2() {
        Array vetor = new Array();
        int expected = 0;
        int result = vetor.percentagemAlgarismosParesVetor(new int[]{1, 3, 3});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void percentagemAlgarismosParesVetor_Test3() {
        Array vetor = new Array();
        int expected = 83;
        int result = vetor.percentagemAlgarismosParesVetor(new int[]{26, 21, 88});
        Assert.assertEquals(expected, result);
    }

    //===============================================================================================
    @Test
    public void elementosComPercentagemMaiorDePares() {
        Array vetor = new Array();
        int[] expected = {668};
        int[] result = vetor.elementosComPercentagemMaiorDePares(new int[]{12, 15, 668});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void elementosComPercentagemMaiorDePares_Test2() {
        Array vetor = new Array();
        int[] expected = {};
        int[] result = vetor.elementosComPercentagemMaiorDePares(new int[]{13, 15, 19});
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void elementosComPercentagemMaiorDePares_Test3() {
        Array vetor = new Array();
        int[] expected = {};
        int[] result = vetor.elementosComPercentagemMaiorDePares(new int[]{22, 48, 626});
        Assert.assertArrayEquals(expected, result);
    }

    //=======================================================================================

    @Test
    public void somenteAlgarismosPares() {
        Array vetor = new Array(22, 48, 626);
        int[] expected = {22, 48, 626};
        int[] result = vetor.somenteAlgarismosPares();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void somenteAlgarismosPares_Test2() {
        Array vetor = new Array(12, 16, 90);
        int[] expected = {};
        int[] result = vetor.somenteAlgarismosPares();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void somenteAlgarismosPares_Test3() {
        Array vetor = new Array(11, 99, 77);
        int[] expected = {};
        int[] result = vetor.somenteAlgarismosPares();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void somenteAlgarismosPares_Test4() {
        Array vetor = new Array();
        int[] expected = {};
        int[] result = vetor.somenteAlgarismosPares();
        Assert.assertArrayEquals(expected, result);
    }

    //===========================================================================================================

    @Test
    public void eCapicua() {
        Array vetor = new Array();
        boolean result = vetor.eCapicua(1125);
        Assert.assertFalse(result);
    }

    @Test
    public void eCapicua_Test2() {
        Array vetor = new Array();
        boolean result = vetor.eCapicua(11);
        Assert.assertTrue(result);
    }

    @Test
    public void eCapicua_Test3() {
        Array vetor = new Array();
        boolean result = vetor.eCapicua(112211);
        Assert.assertTrue(result);
    }

    //==================================================================================

    @Test
    public void testECapicua() {
        Array vetor = new Array(11, 22, 33);
        int[] expected = {11, 22, 33};
        int[] result = vetor.eCapicua();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void testECapicua_Test2() {
        Array vetor = new Array(12, 15, 669);
        int[] expected = {};
        int[] result = vetor.eCapicua();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void testECapicua_Test3() {
        Array vetor = new Array();
        int[] expected = {};
        int[] result = vetor.eCapicua();
        Assert.assertArrayEquals(expected, result);
    }

    //==================================================================================================

    @Test
    public void elementosDoVetorComNumerosIguais() {
        Array vetor = new Array(16, 12, 11);
        int[] expected = {11};
        int[] result = vetor.elementosDoVetorComNumerosIguais();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void elementosDoVetorComNumerosIguais_Test2() {
        Array vetor = new Array(55, 22, 33);
        int[] expected = {55, 22, 33};
        int[] result = vetor.elementosDoVetorComNumerosIguais();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void elementosDoVetorComNumerosIguais_Test3() {
        Array vetor = new Array(1, 2, 3);
        int[] expected = {};
        int[] result = vetor.elementosDoVetorComNumerosIguais();
        Assert.assertArrayEquals(expected, result);
    }

    //===========================================================================================

    @Test
    public void eIgual() {
        Array vetor = new Array(22, 3569, 355);
        boolean result = vetor.eIgual(22);
        Assert.assertTrue(result);
    }

    @Test
    public void eIgual_Test2() {
        Array vetor = new Array(11, 2, 0);
        boolean result = vetor.eIgual(0);
        Assert.assertFalse(result);
    }

    @Test
    public void eIgual_Test3() {
        Array vetor = new Array(0, 2, 3);
        boolean result = vetor.eIgual(2);
        Assert.assertFalse(result);
    }

    //===================================================================================

    @Test
    public void vetorIgual() {
        Array vetor = new Array(1, 2, 3);
        boolean result = vetor.vetorIgual(new int[]{1, 2, 3});
        Assert.assertTrue(result);

    }

    @Test
    public void vetorIgual_Test2() {
        Array vetor = new Array();
        boolean result = vetor.vetorIgual(new int[]{});
        Assert.assertTrue(result);

    }

    @Test
    public void vetorIgual_Test3() {
        Array vetor = new Array(10, 2, 3);
        boolean result = vetor.vetorIgual(new int[]{1, 2, 3});
        Assert.assertFalse(result);

    }
}
