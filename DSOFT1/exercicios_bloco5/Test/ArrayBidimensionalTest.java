import org.junit.Assert;
import org.junit.Test;

public class ArrayBidimensionalTest {

    @Test
    public void matrizEVazia() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[0][0]);
        boolean expected = matriz.matrizEVazia();
        Assert.assertTrue(expected);
    }

    @Test
    public void matrizEVazia_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2}, {2}});
        boolean expected = matriz.matrizEVazia();
        Assert.assertFalse(expected);
    }

    @Test(expected = NullPointerException.class)
    public void matrizEVazia_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional(null);
        boolean result = matriz.matrizEVazia();
    }

    //================================================================================================================

    @Test
    public void maiorElementoMatriz() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2, 3}, {1, 9, 8}, {10, 8, 7}});
        int expected = 10;
        int result = matriz.maiorElementoMatriz();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void maiorElementoMatriz_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2, 3}, {1, 9, 8}, {-10, 8, 7}});
        int expected = 9;
        int result = matriz.maiorElementoMatriz();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void maiorElementoMatriz_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2, 3}, {1}, {10, 8, 7, 20, 25, 30}});
        int expected = 30;
        int result = matriz.maiorElementoMatriz();
        Assert.assertEquals(expected, result);
    }

    //===================================================================================================
    @Test
    public void menorElementoMatriz() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2, 3}, {0}, {10, 8, 7, 20, 25, 30}});
        int expected = 0;
        int result = matriz.menorElementoMatriz();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void menorElementoMatriz_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2, 3}, {1, 9, 8}, {-10, 8, 7}});
        int expected = -10;
        int result = matriz.menorElementoMatriz();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void menorElementoMatriz_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2, 3}, {1, 9, 8}, {10, 8, 7}});
        int expected = 1;
        int result = matriz.menorElementoMatriz();
        Assert.assertEquals(expected, result);
    }

    //==================================================================================================================

    @Test
    public void mediaElementosMatriz() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2, 3, 4}, {2, 4, 8, 9}, {3, 5, 7, 3}});
        int expected = 3;
        int result = matriz.mediaElementosMatriz();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaElementosMatriz_Teste2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 4}, {2, 4, 8, 9}, {3, 5, 7}});
        int expected = 4;
        int result = matriz.mediaElementosMatriz();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaElementosMatriz_Teste3() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[0][0]);
        int expected = 0;
        int result = matriz.mediaElementosMatriz();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaElementosMatriz_Teste4() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{-4, 2}, {-2, -8}});
        int expected = -3;
        int result = matriz.mediaElementosMatriz();
        Assert.assertEquals(expected, result);
    }

    //============================================================================================================

    @Test
    public void retornaSomaDeCadaLinha() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 3, 5}, {2, 6, 9}, {7, 9, 1}});
        int[] expected = {10, 17, 17};
        int[] result = matriz.retornaSomaDeCadaLinha();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void retornaSomaDeCadaLinha_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 3, 1}, {0, 1}, {-7, -6}});
        int[] expected = {6, 1, -13};
        int[] result = matriz.retornaSomaDeCadaLinha();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void retornaSomaDeCadaLinha_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[0][0]);
        int[] expected = {};
        int[] result = matriz.retornaSomaDeCadaLinha();
        Assert.assertArrayEquals(expected, result);
    }

    //======================================================================================================

    @Test
    public void retornaTamanhoDaMaiorLinhaDaMatriz() {
        ArrayBidimensional matriz = new ArrayBidimensional();
        int expected = 3;
        int result = matriz.retornaTamanhoDaMaiorLinhaDaMatriz(new int[][]{{2, 3, 1}, {0, 1, 3}, {-7, -6, 3}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void retornaTamanhoDaMaiorLinhaDaMatriz_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional();
        int expected = 6;
        int result = matriz.retornaTamanhoDaMaiorLinhaDaMatriz(new int[][]{{2, 3, 1}, {0, 1}, {-7, -6}, {6, 3, 9, 5, 2, 1}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void retornaTamanhoDaMaiorLinhaDaMatriz_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional();
        int expected = 0;
        int result = matriz.retornaTamanhoDaMaiorLinhaDaMatriz(new int[0][0]);
        Assert.assertEquals(expected, result);
    }

    //=============================================================================================================
    @Test
    public void retornaSomaDeCadaColuna() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 3}, {0, 1}, {-7, -6}, {6, 3}});
        int[] expected = {1, 1};
        int[] result = matriz.retornaSomaDeCadaColuna();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void retornaSomaDeCadaColuna_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 3}, {0, 1, 6}, {-7, -6}, {6, 3, 2}});
        int[] expected = {1, 1, 8};
        int[] result = matriz.retornaSomaDeCadaColuna();
        Assert.assertArrayEquals(expected, result);
    }

    //===============================================================================================================
    @Test
    public void retornaIndiceDoVetorDeMaiorSoma() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 3}, {0, 1, 6}, {-7, -6}, {6, 3, 2}});
        int expected = 3;
        int result = matriz.retornaIndiceDoVetorDeMaiorSoma();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void retornaIndiceDoVetorDeMaiorSoma_Test2() {//está condicionado para que seja assim, mas como poderia tratar?
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{6, 3}, {6, 3}, {6, 3}, {6, 3}});
        int expected = 0;
        int result = matriz.retornaIndiceDoVetorDeMaiorSoma();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void retornaIndiceDoVetorDeMaiorSoma_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 3}, {0, 1}, {7, 6}, {6, 3}});
        int expected = 2;
        int result = matriz.retornaIndiceDoVetorDeMaiorSoma();
        Assert.assertEquals(expected, result);
    }

    //================================================================================================================

    @Test
    public void verificaSeEhSimetrica() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2}, {2, 1}});
        boolean result = matriz.verificaMatrizSimetrica();
        Assert.assertTrue(result);
    }

    @Test
    public void verificaSeEhSimetrica_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2, 3}, {2, 3, 1}, {3, 2, 1}});
        boolean result = matriz.verificaMatrizSimetrica();
        Assert.assertFalse(result);
    }

    //=================================================================================================================
    @Test
    public void quantElementosDiagonalPrincipal() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 3}, {0, 1}});
        int expected = 2;
        int result = matriz.quantElementosDiagonalPrincipal();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void quantElementosDiagonalPrincipal_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 3}, {0, 1}, {2, 3}});
        int expected = -1;
        int result = matriz.quantElementosDiagonalPrincipal();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void quantElementosDiagonalPrincipal_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{22, 35, 40}, {80, 1, 11}, {92, 663, 776}});
        int expected = 3;
        int result = matriz.quantElementosDiagonalPrincipal();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void quantElementosDiagonalPrincipal_Test4() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[0][0]);
        int expected = -1;
        int result = matriz.quantElementosDiagonalPrincipal();
        Assert.assertEquals(expected, result);
    }

    //==================================================================================================================
    @Test
    public void diagonaisIguais() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 2}, {2, 1}});
        boolean result = matriz.diagonaisIguais();
        Assert.assertFalse(result);
    }

    @Test
    public void diagonaisIguais_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 0, 1}, {0, 2, 0}, {3, 0, 3}});
        boolean result = matriz.diagonaisIguais();
        Assert.assertTrue(result);
    }

    @Test
    public void diagonaisIguais_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[0][0]);
        boolean result = matriz.diagonaisIguais();
        Assert.assertFalse(result);
    }

    //==================================================================================================================
    @Test
    public void mediaAlgarismosMatriz() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 0, 1}, {0, 2, 0}, {3, 0, 3}});
        int expected = 1;
        int result = matriz.mediaAlgarismosMatriz();
        Assert.assertEquals(expected, result);
    }

    @Test
    public void mediaAlgarismosMatriz_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{122, 0, 1}, {0, 22, 0}, {3, 10, 3}});
        int expected = 1;
        int result = matriz.mediaAlgarismosMatriz();
        Assert.assertEquals(expected, result);
    }

    //==================================================================================================================
    @Test
    public void algarismosAcimaDaMediaMatriz() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{122, 0, 1}, {0, 22, 0}, {3, 10, 3}});
        int[] expected = new int[]{122, 22, 10};
        int[] result = matriz.algarismosAcimaDaMediaMatriz();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void algarismosAcimaDaMediaMatriz_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{122, 0, 1}, {0, 22, 0}, {99}});
        int[] expected = new int[]{122, 22, 99};
        int[] result = matriz.algarismosAcimaDaMediaMatriz();
        Assert.assertArrayEquals(expected, result);
    }

    //==============================================================================================================

    @Test
    public void percentagemDeParesNaMatriz() {
        ArrayBidimensional matriz = new ArrayBidimensional();
        int expected = 61;
        int result = matriz.percentagemDeParesNaMatriz(new int[][]{{122, 0, 1}, {0, 22, 0}, {3, 10, 3}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void percentagemDeParesNaMatriz_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional();
        int expected = 0;
        int result = matriz.percentagemDeParesNaMatriz(new int[][]{{1, 33}, {15, 13, 97}});
        Assert.assertEquals(expected, result);
    }

    @Test
    public void percentagemDeParesNaMatriz_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional();
        int expected = 100;
        int result = matriz.percentagemDeParesNaMatriz(new int[][]{{2, 4600, 88}, {20, 26, 24}});
        Assert.assertEquals(expected, result);
    }

    //====================================================================================================================
    @Test
    public void elementosComMaiorPercentagemPares() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 4600, 88}, {20, 26, 24}});
        int[] expected = {};
        int[] result = matriz.elementosComMaiorPercentagemParesMatriz();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void elementosComMaiorPercentagemPares_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{122, 0, 1}, {0, 22, 0}, {3, 10, 3}});
        int[] expected = {122, 0, 0, 22, 0};
        int[] result = matriz.elementosComMaiorPercentagemParesMatriz();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void elementosComMaiorPercentagemPares_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{22, 35, 40}, {80, 1, 11}, {92, 663, 776}});
        int[] expected = {22, 40, 80, 663};
        int[] result = matriz.elementosComMaiorPercentagemParesMatriz();
        Assert.assertArrayEquals(expected, result);
    }
    //====================================================================================================================

    @Test
    public void percentagemParesNumInteiro() {
        ArrayBidimensional matriz = new ArrayBidimensional();
        int expected = 50;
        int result = matriz.percentagemParesNumInteiro(10);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void percentagemParesNumInteiro_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional();
        int expected = 100;
        int result = matriz.percentagemParesNumInteiro(268);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void percentagemParesNumInteiro_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional();
        int expected = 0;
        int result = matriz.percentagemParesNumInteiro(39751);
        Assert.assertEquals(expected, result);
    }

    //====================================================================================================================

    @Test
    public void inverterOrdemDasLinhas() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 4600, 88}, {20, 26, 24}});
        int[][] expected = {{88, 4600, 2}, {24, 26, 20}};
        int[][] result = matriz.inverterOrdemDasLinhas();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void inverterOrdemDasLinhas_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{22, 35, 40}, {80, 1}, {92, 663, 776}});
        int[][] expected = {{40, 35, 22}, {1, 80}, {776, 663, 92}};
        int[][] result = matriz.inverterOrdemDasLinhas();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void inverterOrdemDasLinhas_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 4600, 88}, {20, 26, 24}});
        int[][] expected = {{88, 4600, 2}, {24, 26, 20}};
        int[][] result = matriz.inverterOrdemDasLinhas();
        Assert.assertArrayEquals(expected, result);
    }

    //=================================================================================================================
    @Test
    public void inverterOrdemDasColunas() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{2, 4600, 88}, {20, 26, 24}});
        int[][] expected = {{20, 26, 24}, {2, 4600, 88}};
        int[][] result = matriz.inverterOrdemDasColunas();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void inverterOrdemDasColunas_Test2() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{122, 0, 1}, {0, 22, 0}, {3, 10, 3}});
        int[][] expected = {{3, 10, 3}, {0, 22, 0}, {122, 0, 1}};
        int[][] result = matriz.inverterOrdemDasColunas();
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void inverterOrdemDasColunas_Test3() {
        ArrayBidimensional matriz = new ArrayBidimensional(new int[][]{{1, 0, 1}, {0, 2, 0}, {3, 0, 3}});
        int[][] expected = {{3, 0, 3}, {0, 2, 0}, {1, 0, 1}};
        int[][] result = matriz.inverterOrdemDasColunas();
        Assert.assertArrayEquals(expected, result);
    }

}