package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Exercicio15Test {

    @Test
    public void retornaMatrizTransposta() {
        int[][] expected = new int[][]{{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
        int[][] result = Exercicio15.retornaMatrizTransposta(new int[][]{{1, 1, 1}, {2, 2, 2}, {3, 3, 3}});
        Assert.assertArrayEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void retornaMatrizTransposta_Test2() {
        int[][] result = Exercicio15.retornaMatrizTransposta(new int[][]{{1, 1, 1}, {2, 2}, {3, 3, 3}});
    }

    @Test
    public void retornaMatrizTransposta_Test3() {
        int[][] expected = new int[][]{{1, 3}, {1, 3}, {1, 3}};
        int[][] result = Exercicio15.retornaMatrizTransposta(new int[][]{{1, 1, 1}, {3, 3, 3}});
        Assert.assertArrayEquals(expected, result);
    }
}