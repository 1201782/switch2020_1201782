package bloco4;

class Exercicio1 {
    /**
     * Descrição: Encontra o número de dígitos de um inteiro
     *
     * @param numero
     * @return
     */
    public static int descobreNumeroDeDigitosInt(int numero) {
        int quantidadeNumeros = 0;
        if (numero == 0) {
            quantidadeNumeros = 1;
        } else if (numero < 0) {
            numero = -numero;
        }
        while (numero > 0) {
            numero = numero / 10;
            quantidadeNumeros++;
        }
        return quantidadeNumeros;
    }
}
