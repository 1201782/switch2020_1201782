package bloco4;

public class Exercicio18 {
    /*public static void main(String[] args) {
        existePalavra("amor");
    }*/

    public static int[][] matrizMascaraDeUmaLetra(char[][] matriz, char letra) {
        int[][] matrizMascaraDeUmaLetra = new int[matriz.length][matriz[0].length];

        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                if (matriz[linha][coluna] == letra) {
                    matrizMascaraDeUmaLetra[linha][coluna] = 1;
                } else {
                    matrizMascaraDeUmaLetra[linha][coluna] = 0;
                }
            }
        }
        return matrizMascaraDeUmaLetra;
    }

    public static boolean existePalavra(String palavra) {
        /*
        matriz letras
        a b a c a t e x
        m b u r a c o e
        e l a m a r i c
        r i p c l p r l
        m e l a a x z i
        c h q r t x l t
        z e b r a i m a

        palavras: abacate, abacaxi, buraco, zebra, mel, mar, ela, lata.
        amor
        primeiro pega a primeira e verifica se há a segunda em alguma das direções, se não, passa pra
        proxima incidencia
        */
        char []letrasDaPalavra = new char[palavra.length()];
        for (int item = 0; item < palavra.length(); item++) {
            letrasDaPalavra[item] = palavra.charAt(item);
        }
        int pos =0;
        char [][] matrizLetras = new char[][]{
                {'a', 'b', 'a', 'c', 'a', 't', 'e','x'},
                {'m', 'b', 'u', 'r', 'a', 'c', 'o','e'},
                {'e', 'l', 'a', 'm', 'a', 'r', 'i','c'},
                {'r', 'i', 'p', 'c', 'l', 'p', 'r','l'},
                {'m', 'e', 'l', 'a', 'a', 'x', 'z','i'},
                {'c', 'h', 'q', 't', 't', 'x', 'l','t'},
                {'z', 'e', 'b', 'r', 'a', 'i', 'm','a'},
        };
        for (int linha = 0; linha <matrizLetras.length; linha++) {
            for (int coluna = 0; coluna < matrizLetras[0].length; coluna++) {
                if(letrasDaPalavra[pos]==matrizLetras[linha][coluna]){
                    //verifica se da esquerda pra direita existe a próxima letra
                    //verifica se de cima pra baixo existe a próxima letra
                    //verifica se da direita pra esquerda existe
                    //verifica se de baixo pra cima existe
                    //verifica se na diagonal crescente esquerda
                    //diagonal decrescente esquerda
                    //diagonal crescente direita direita
                    //diagonal decrescnte direita
                }
            }
        }

        return true;
    }
    public static boolean verificaEsqDireita(int [][]matriz, char letra){

        return false;
    }

}
