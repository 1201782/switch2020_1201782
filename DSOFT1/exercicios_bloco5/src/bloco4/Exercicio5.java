package bloco4;

public class Exercicio5 {
    /**
     * Descrição: soma os dígitos ímpares de um inteiro
     *
     * @param numero
     * @return
     */
    public static int somaNumerosPares(int numero) {
        if(numero<0){
            numero=-numero;
        }
        int soma = 0;
        while (numero > 0) {
            int digito = numero % 10;
            if (digito % 2 == 0) {
                soma += digito;
            }
            numero = numero / 10;
        }
        return soma;
    }

    /**
     * Descrição: soma os dígitos ímpares de um inteiro
     *
     * @param numero
     * @return
     */
    public static int somaNumerosImpares(int numero) {
        if(numero<0){
            numero=-numero;
        }
        int soma = 0;
        while (numero > 0) {
            int digito = numero % 10;
            if (digito % 2 != 0) {
                soma += digito;
            }
            numero = numero / 10;
        }
        return soma;
    }
}
