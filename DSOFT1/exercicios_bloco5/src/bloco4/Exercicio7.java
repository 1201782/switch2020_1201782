package bloco4;

public class Exercicio7 {
    /**
     * Descrição: a partir de um intervalo encontra os multiplos de um dado número
     *
     * @param intMin
     * @param intMax
     * @param numero
     * @return
     */
    public static int[] multiplosDeN(int intMin, int intMax, int numero) {
        if (numero == 0) {
            return null;
        }
        if(numero<0){
            numero= -numero;
        }
        int[] multiplosDeN = new int[intMax - intMin];
        int tamanhoVetor = 0;
        for (int intervalo = intMin; intervalo <= intMax; intervalo++) {
            if (intervalo % numero == 0) {
                multiplosDeN[tamanhoVetor] = intervalo;
                tamanhoVetor++;
            }
        }
        int[] resultado = new int[tamanhoVetor];
        for (int indexResultado = 0; indexResultado < tamanhoVetor; indexResultado++) {
            resultado[indexResultado] = multiplosDeN[indexResultado];
        }
        return resultado;
    }
}
