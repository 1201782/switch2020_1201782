package bloco4;

public class Exercicio4 {
    /**
     * Descrição: A partir de um vetor, cria um novo array só com os números pares
     *
     * @param entrada
     * @return
     */
    public static int[] separarPares(int[] entrada) {

        int [] numerosPares = new int[entrada.length];
        int j = 0;

        for (int item : entrada) {
            if (item % 2 == 0) {
                numerosPares[j] = item;
                j++;
            }
        }
        int [] resultado = new int[j];
        for (int index = 0; index < j; index++) {
            resultado [index]= numerosPares[index];
        }
        return resultado;
    }

    /**
     * Descrição: A partir de um vetor, cria um novo array só com os números ímpares
     *
     * @param entrada
     * @return
     */
    public static int[] separarImpares(int[] entrada) {

        int [] numerosImpares=new int[entrada.length];
        int contador = 0;
        for (int index = 0; index < (entrada.length); index++) {
            if(entrada[index] %2 !=0){
                numerosImpares[contador] = entrada[index];
                contador++;
            }
        }
        int [] resultado = new int[contador];
        for (int index = 0; index < contador; index++) {
            resultado [index]= numerosImpares[index];
        }
        return resultado;
    }


}
