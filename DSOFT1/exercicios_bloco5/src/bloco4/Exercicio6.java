package bloco4;

public class Exercicio6 {
    /**
     * Descrição: Retorna um vetor com os primeiros n elementos de outro vetor
     *
     * @param entrada
     * @param numero
     * @return
     */
    public static int[] vetorMenor(int[] entrada, int numero) {
        if (numero > entrada.length || numero < 0) {
            return null;
        }
        int[] novoVetor = new int[numero];
        for (int index = 0; index < numero; index++) {
            novoVetor[index] = entrada[index];
        }
        return novoVetor;
    }
}
