package bloco4;

public class Exercicio9 {
    /**
     * Descrição: verifica se o inteiro é capicua
     *
     * @param numero
     * @return
     */
    public static boolean eCapicua(int numero) {
        int[] digitos = new int[10];
        boolean eCapicua;
        int tamanhoVetor = 0;
        while (numero > 0) {
            digitos[tamanhoVetor] = numero % 10;
            numero = numero / 10;
            tamanhoVetor++;
        }
        int[] resultado = new int[tamanhoVetor];
        for (int pos = 0; pos < tamanhoVetor; pos++) {
            resultado[pos] = digitos[pos];
        }
        int[]arrayInvertido=criaArrayInvertido(resultado);
        if (resultado.length == arrayInvertido.length) {
            int size = resultado.length - 1;
            while (size >= 0) {
                if (resultado[size] != arrayInvertido[size]) {
                    return false;
                }
                size--;
            }
            eCapicua = true;
        } else {
            eCapicua = false;
        }
        return eCapicua;
    }
    public static int[] criaArrayInvertido(int[]resultado){
        int index = 0;
        int[] arrayInvertido = new int[resultado.length];
        for (int pos = resultado.length; pos > 0; pos--) {
            arrayInvertido[index] = resultado[pos - 1];
            index++;
        }
        return arrayInvertido;
    }
}
