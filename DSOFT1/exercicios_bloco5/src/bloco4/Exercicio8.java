package bloco4;

public class Exercicio8 {
    /**
     * Descrição: a partir de um intervalo encontra os multiplos comuns de
     * elementos de um array
     *
     * @param min
     * @param max
     * @param vetor
     * @return
     */
    public static int[] multiplosComuns(int min, int max, int[] vetor) {
        if (vetor.length == 0) {
            return null;
        }
        int[] novoArray = new int[(max - min) + 1];
        int contador = 0;
        int divisiveis = 0;
        for (int linha = min; linha <= max; linha++) {
            for (int coluna = 0; coluna < vetor.length; coluna++) {
                if (linha % vetor[coluna] == 0) {
                    divisiveis++;
                }
            }
            if (divisiveis == vetor.length) {
                novoArray[contador] = linha;
                contador++;
            }
            divisiveis = 0;
        }
        int[] resultado = new int[contador];
        for (int index = 0; index < contador; index++) {
            resultado[index] = novoArray[index];
        }
        return resultado;
    }
}
