package bloco4;

public class Exercicio10 { //falta refinar código
    /**
     * Descrição: encontra o menor inteiro de um vetor
     * @param inteiros
     * @return
     */
    public static int vetorMenorValor(int[] inteiros) {
        int menor = 0;
        for (int inteiro : inteiros) {
            if (inteiro > menor) {
                menor = inteiro;
            }
        }
        return menor;
    }

    /**
     * Descrição: encontra o maior inteiro de um vetor
     * @param inteiros
     * @return
     */
    public static int vetorMaiorValor(int[] inteiros) {
        int maior = 0;
        for (int inteiro : inteiros) {
            if (inteiro > maior) {
                maior = inteiro;
            }
        }
        return maior;
    }

    /**
     * Descrição: encontra o valor médio dos elementos de um vetor
     * @param inteiros
     * @return
     */
    public static int valorMedioVetor(int[] inteiros) {
        int totalInteiros = 0;
        int somaInteiros = 0;
        for (int inteiro : inteiros) {
            somaInteiros += inteiro;
            totalInteiros++;
        }
        return somaInteiros / totalInteiros;
    }

    /**
     * Descrição: encontra o produto dos elementos de um vetor
     * @param inteiros
     * @return
     */
    public static int produtoVetor(int[] inteiros) {
        int produtosVetores = 1;
        for (int inteiro : inteiros) {
            produtosVetores *= inteiro;
        }
        return produtosVetores;
    }

    /**
     * Descrição: encontra os elementos únicos de um vetor
     * @param inteiros
     * @return
     */
    public static int[] elementosUnicosNoVetor(int[] inteiros) {
        int index = 0;
        int ocorrencias = 0;
        int resultadosencontrados = 0;
        int[] elementosUnicos = new int[inteiros.length];

        for (int item : inteiros) {
            if (index == 0) {
                elementosUnicos[index] = item;
                index++;
                resultadosencontrados++;
            } else {
                for (int itemAux : elementosUnicos) {
                    if (item == itemAux) {
                        ocorrencias++;
                    }
                }
                if (ocorrencias == 0) {
                    elementosUnicos[index] = item;
                    index++;
                    resultadosencontrados++;
                }
                ocorrencias = 0;
            }
        }
        int[] resultado = new int[resultadosencontrados];
        for (int i = 0; i < resultadosencontrados; i++) {
            resultado[i] = elementosUnicos[i];
        }
        return resultado;
    }

    /**
     * Descrição: inverte a ordem dos elementos de um vetor
     * @param numeros
     * @return
     */
    public static int[] ordemInversaVetor(int[] numeros) {
        int[] vetorOrdemInversa = new int[numeros.length];
        int indexNumeros = 0;
        for (int index = numeros.length; index >= 0; index--) {
            vetorOrdemInversa[index] = numeros[indexNumeros];
            indexNumeros++;
        }
        return vetorOrdemInversa;
    }

    //falta exercicio dos elementos primos

}
