import java.util.Arrays;

public class Array {

    private int[] vetorInt;

    //construtores
    public Array(int valor1, int valor2, int valor3) {
        this.vetorInt = new int[]{valor1, valor2, valor3};
    }

    public Array() {
        vetorInt = new int[0];
    }

    //metodos

    /**
     * copia vetor
     *
     * @param tamanho
     * @return
     */
    private int[] copiarVetor(int tamanho) {
        int[] copia = new int[tamanho];
        for (int index = 0; (index < tamanho && index < this.vetorInt.length); index++) {
            copia[index] = vetorInt[index];
        }
        return copia;
    }

    /**
     * adiciona elemento no vetor
     *
     * @param elemento
     */
    public void adicionarElemento(int elemento) {
        this.vetorInt = copiarVetor(vetorInt.length + 1);
        this.vetorInt[vetorInt.length - 1] = elemento;
    }

    /**
     * retira primeiro elemento do vetor
     *
     * @param vetor
     */
    public void retiraPrimeiroElemento(int[] vetor) {
        int[] novoVetor = new int[vetorInt.length - 1];
        for (int elemento = 0; elemento < vetorInt.length - 1; elemento++) {
            novoVetor[elemento] = vetorInt[elemento + 1];
        }
        this.vetorInt = novoVetor;
    }

    /**
     * retorna o elemento na posição desejada
     *
     * @param posicao
     * @return
     */
    public int retornaElementoNaPosicaoXDoVetor(int posicao) {
        if (posicao > vetorInt.length - 1 || posicao < 0) {
            throw new IllegalArgumentException("Esta posição não existe no vetor");
        }
        return vetorInt[posicao];
    }

    /**
     * retorna o número de elementos de um vetor
     *
     * @param vetorInteiros
     * @return
     */
    public int retornaNumeroElementosVetor(int[] vetorInteiros) {
        this.vetorInt = vetorInteiros;
        return vetorInteiros.length;
    }

    /**
     * soma elementos de um vetor
     *
     * @param vetorInteiro
     * @return
     */
    public int somaElementosVetor(int[] vetorInteiro) {
        this.vetorInt = vetorInteiro;
        int soma = 0;
        for (int elemento : vetorInteiro) {
            soma += elemento;
        }
        if (vetorInteiro == null) {
            soma = 0;
        }
        return soma;
    }

    /**
     * retorna o maior elemento de um vetor
     *
     * @param vetorInteiro
     * @return
     */
    public int retornaMaiorElementoVetor(int[] vetorInteiro) {
        this.vetorInt = vetorInteiro;
        int maiorElemento = vetorInt[0];
        for (int item : vetorInt) {
            if (maiorElemento < item) {
                maiorElemento = item;
            }
        }
        return maiorElemento;
    }

    /**
     * retorna menor elemento de um vetor
     *
     * @param vetorInteiro
     * @return
     */
    public int retornaMenorElementoVetor(int[] vetorInteiro) {
        this.vetorInt = vetorInteiro;
        int menorElemento = this.vetorInt[0];
        for (int item : vetorInt) {
            if (menorElemento > item) {
                menorElemento = item;
            }
        }
        return menorElemento;
    }

    /**
     * retorna a média dos elementos de um vetor
     *
     * @param vetorInteiro
     * @return
     */
    public int mediaDosElementosDoVetor(int[] vetorInteiro) {
        this.vetorInt = vetorInteiro;
        int somaElementos = somaElementosVetor(vetorInteiro);
        return somaElementos / retornaNumeroElementosVetor(vetorInteiro);
    }

    /**
     * retorna média de elementos pares de um vetor
     *
     * @return
     */
    public int mediaDosElementosParesDoVetor() {
        int soma = 0;
        int contadorDePares = 0;
        int mediaPares;
        for (int elemento :
                vetorInt) {
            if (elemento % 2 == 0) {
                soma += elemento;
                contadorDePares++;
            }
        }
        if (contadorDePares == 0) {
            mediaPares = 0;
        } else {
            mediaPares = soma / contadorDePares;
        }
        return mediaPares;
    }

    /**
     * retorna média de elementos ímpares de um vetor
     *
     * @return
     */
    public int mediaDosElementosImparesDoVetor() {
        int soma = 0;
        int contadorDeImpares = 0;
        int mediaImpares;
        for (int elemento :
                vetorInt) {
            if (elemento % 2 != 0) {
                soma += elemento;
                contadorDeImpares++;
            }
        }
        if (contadorDeImpares == 0) {
            mediaImpares = 0;
        } else {
            mediaImpares = soma / contadorDeImpares;
        }
        return mediaImpares;
    }

    /**
     * retorna a média dos múltiplos de determinado inteiro
     *
     * @param numero
     * @return
     */
    public int mediaDosMultiplosDeNumero(int numero) {
        int soma = 0;
        int contadorDeMultiplos = 0;
        int media;
        for (int elemento : vetorInt) {
            if (elemento % numero == 0) {
                soma += elemento;
                contadorDeMultiplos++;
            }
        }
        if (contadorDeMultiplos == 0) {
            media = 0;
        } else {
            media = soma / contadorDeMultiplos;
        }
        return media;
    }

    /**
     * ordena de forma crescente um vetor
     *
     * @param vetor
     */
    public void ordernarVetorCresc(int[] vetor) {
        this.vetorInt = vetor;
        for (int numero = 0; numero < vetor.length; numero++) {
            if (vetorInt[numero] > vetorInt[numero + 1]) {
                if (numero == vetorInt.length - 1) {
                    vetorInt[numero] = vetorInt[numero];
                } else {
                    int numeroMaior = vetorInt[numero];
                    vetorInt[numero] = vetorInt[numero + 1];
                    vetorInt[numero + 1] = numeroMaior;
                }
            }
        }
    }

    /**
     * ordena o vetor de forma decrescente
     *
     * @param vetor
     */
    public void ordernarVetorDesc(int[] vetor) {
        this.vetorInt = vetor;
        for (int numero = 0; numero < vetorInt.length; numero++) {
            if (vetorInt[numero] < vetorInt[numero + 1]) {
                if (numero == vetorInt.length - 1) {
                    vetorInt[numero] = vetorInt[numero];
                } else {
                    int numeroMenor = vetorInt[numero];
                    vetorInt[numero] = vetorInt[numero + 1];
                    vetorInt[numero + 1] = numeroMenor;
                }
            }
        }
    }

    /**
     * verifica se  o vetor é vazio
     *
     * @return
     */
    public boolean verificaSeVetorEVazio() {
        return vetorInt.length == 0;
    }

    /**
     * verifica se existe apenas um elemento no vetor
     *
     * @return
     */
    public boolean verificaSeTemApenasUmElemento() {
        return vetorInt.length == 1;
    }

    /**
     * verifica se o vetor só tem numeros pares
     *
     * @return
     */
    public boolean verificaSeVetorSotemPares() {
        int contadorPares = 0;
        for (int elemento : vetorInt) {
            if (elemento % 2 == 0) {
                contadorPares++;
            }
        }
        return contadorPares == vetorInt.length;
    }

    /**
     * verifica se o vetor só tem ímpares
     *
     * @return
     */
    public boolean verificaSeVetorSotemImpares() {
        int contadorImpares = 0;
        for (int elemento : vetorInt) {
            if (elemento % 2 != 0) {
                contadorImpares++;
            }
        }
        return contadorImpares == vetorInt.length;
    }

    /**
     * verifica se há elementos repetidos
     *
     * @return
     */
    public boolean verificaElementosRepetidos() {
        int posicao = 0;
        boolean haRepetido = false;
        while (posicao < vetorInt.length) {
            for (int numero = 0; numero < vetorInt.length; numero++) {
                if (vetorInt[posicao] == vetorInt[numero]) {
                    if (posicao != numero) {
                        haRepetido = true;
                    }
                }
            }
            posicao++;
        }
        return haRepetido;
    }

    /**
     * retorna a média dos algarismos do vetor
     *
     * @return
     */
    public int mediaAlgarismosDoVetor(int[] vetorInteiros) {
        this.vetorInt = vetorInteiros;
        int media;
        if (vetorInt.length > 0) {
            media = Math.round(totalAlgarismosVetor(vetorInt) / vetorInt.length);
        } else {
            media = 0;
        }
        return media;
    }

    /**
     * conta quantos algarismos há no vetor
     *
     * @return
     */
    public int totalAlgarismosVetor(int[] vetorInteiro) {
        this.vetorInt = vetorInteiro;
        int somaQuantDigitos = 0;
        for (int elemento : vetorInt) {
            somaQuantDigitos += numeroDeDigitosInt(elemento);
        }
        return somaQuantDigitos;
    }

    /**
     * conta o número de digitos de um inteiro
     *
     * @param numero
     * @return
     */
    public int numeroDeDigitosInt(int numero) {
        int quantidadeDigitos = 0;
        //validações
        if (numero == 0) {
            quantidadeDigitos = 1;
        } else if (numero < 0) {
            numero = -numero;
        }
        //operação
        while (numero > 0) {
            numero = numero / 10;
            quantidadeDigitos++;
        }
        return quantidadeDigitos;
    }

    /**
     * retorna um vetor redimensionado
     *
     * @param vetor
     * @param tamanho
     * @return
     */
    public int[] redimensionaVetor(int[] vetor, int tamanho) {
        int[] resultado = new int[tamanho];
        for (int item = 0; item < tamanho; item++) {
            resultado[item] = vetor[item];
        }
        return resultado;
    }

    /**
     * retorna os algarismos que tem média acima da média de algarismos do vetor
     *
     * @return
     */
    public int[] algarismosAcimaDaMedia(int[] vetorInteiros) {
        this.vetorInt = vetorInteiros;
        int tamanho = vetorInt.length;
        int posicao = 0;
        int[] vetorAlgarimosAcimaMedia = new int[tamanho];
        double mediaDeAlgarismos = mediaAlgarismosDoVetor(vetorInt);

        for (int item : vetorInt) {
            if (numeroDeDigitosInt(item) > mediaDeAlgarismos) {
                vetorAlgarimosAcimaMedia[posicao] = item;
                posicao++;
            }
        }
        return redimensionaVetor(vetorAlgarimosAcimaMedia, posicao);
    }

    /**
     * retorna a percentagem de algarismos pares de um vetor
     *
     * @return
     */
    public int percentagemAlgarismosParesVetor(int[] vetorInteiros) {
        this.vetorInt = vetorInteiros;
        int quantTotalAlgarismos = totalAlgarismosVetor(vetorInt);
        int quantidadeDigitosPares = 0;
        int percentagemPares;
        for (int elemento : vetorInt) {
            int numero = elemento;
            if (elemento == 0) {
                quantidadeDigitosPares++;
            }
            while (numero > 0) {
                int digito = 0;
                if (numero >= 10) {
                    digito = numero % 10;
                } else {
                    digito = numero;
                }

                if (digito % 2 == 0 || digito == 0) {
                    quantidadeDigitosPares++;
                }
                numero = numero / 10;
            }
        }
        if (quantTotalAlgarismos != 0) {
            percentagemPares = quantidadeDigitosPares * 100 / quantTotalAlgarismos;
        } else {
            percentagemPares = 0;
        }
        return percentagemPares;
    }

    /**
     * retorna os elementos com percentagem de algarismos pares maior que a média de pares do vetor
     *
     * @return
     */
    public int[] elementosComPercentagemMaiorDePares(int[] vetorInteiros) {
        this.vetorInt = vetorInteiros;
        int[] maisPares = new int[vetorInt.length];
        int posicao = 0;

        int totalPercentPares = percentagemAlgarismosParesVetor(vetorInt);
        int percentagemParesNoElemento = 0;

        int contadorPares = 0;
        int contadorDigitos = 0;

        for (int item : vetorInt) {
            int elemento = item;
            do {
                int digito = elemento % 10;
                if (digito % 2 == 0) {
                    contadorPares++;
                }
                contadorDigitos++;
                elemento /= 10;
            }
            while (elemento > 0);

            if (contadorDigitos != 0) {
                percentagemParesNoElemento = (contadorPares * 100) / contadorDigitos;
            } else {
                percentagemParesNoElemento = 0;
            }
            if (percentagemParesNoElemento > totalPercentPares) {
                maisPares[posicao] = item;
                posicao++;
            }
            contadorDigitos = 0;
            contadorPares = 0;
            percentagemParesNoElemento = 0;
        }

        return redimensionaVetor(maisPares, posicao);
    }

    /**
     * retorna somente os elementos do vetor com todos os algarismos pares
     *
     * @return
     */
    public int[] somenteAlgarismosPares() {
        int[] somenteAlgarismosPares = new int[vetorInt.length];
        int totalDigitos = 0;
        int validador = 0;
        int index = 0;

        for (int numero : vetorInt) {
            int elemento = numero;
            while (elemento > 0) {
                int digito = elemento;
                digito %= 10;
                if (ePar(digito)) {
                    validador++;
                }
                elemento /= 10;
                totalDigitos++;
            }
            if (validador == totalDigitos) {
                somenteAlgarismosPares[index] = numero;
                validador = 0;
                totalDigitos = 0;
                index++;
            }
        }

        return redimensionaVetor(somenteAlgarismosPares, index);
    }

    /**
     * verifica se um número inteiro é par
     *
     * @param numero
     * @return
     */
    public boolean ePar(int numero) {
        return numero % 2 == 0;
    }

   /* public int[] elementosCrescentes() {
        int tamanho = vetorInt.length;
        int[] elementosSequenciais = new int[tamanho];
        int index = 0;
        for (int item : vetorInt) {
            if (eCrescente(item)) {
                elementosSequenciais[index] = item;
            }
        }
        return redimensionaVetor(elementosSequenciais,index);
    }

    public boolean eCrescente(int numero){
        int elemento = numero;
        int[] vetorDeDigitos = new int[vetorInt.length];
        for (int item = 0; item < vetorDeDigitos.length; item++) {
            while (elemento>0){
                vetorDeDigitos[item]= elemento%=10;
                elemento/=10;
            }
        }
        int[]vetorCresc = vetorDeDigitos;
        ordernarVetorCresc(vetorDeDigitos);
        return Arrays.equals(vetorDeDigitos,vetorCresc);
    }*/

    /**
     * verifica se um número é capicua
     *
     * @param numero
     * @return
     */
    public boolean eCapicua(int numero) {
        int numeroDeDigitos = numeroDeDigitosInt(numero);
        String digito = "";
        boolean eCapicua;
        if (numeroDeDigitos % 2 == 0) {
            for (int i = 0; i < (numeroDeDigitos / 2); i++) {
                digito = digito + (numero % 10);
                numero = numero / 10;
            }
            if (numero == Integer.parseInt(digito)) {
                eCapicua = true;
            } else {
                eCapicua = false;
            }
        } else {
            for (int i = 0; i < (numeroDeDigitos / 2); i++) {
                digito += (numero % 10);
                numero = numero / 10;
            }
            numero = numero / 10;
            if (numero == Integer.parseInt(digito)) {
                eCapicua = true;
            } else {
                eCapicua = false;
            }
        }
        return eCapicua;
    }

    /**
     * retorna os elementos do vetor que são capicua
     *
     * @return
     */
    public int[] eCapicua() {
        int[] capicuasDoVetor = new int[vetorInt.length];
        int index = 0;
        for (int item : vetorInt) {
            if (eCapicua(item)) {
                capicuasDoVetor[index] = item;
                index++;
            }
        }

        return redimensionaVetor(capicuasDoVetor, index);
    }

    /**
     * retorna os elementos do vetor que possuem o mesmo algarismo
     *
     * @return
     */
    public int[] elementosDoVetorComNumerosIguais() {
        int index = 0;
        int[] iguais = new int[vetorInt.length];
        for (int item : vetorInt) {
            if (eIgual(item)) {
                iguais[index] = item;
                index++;
            }
        }
        return redimensionaVetor(iguais, index);
    }

    /**
     * verifica se um número inteiro tem algarismos iguais
     *
     * @param numero
     * @return
     */
    public boolean eIgual(int numero) {
        boolean eIgual = false;
        int index = 0;
        int[] vetorDigitos = new int[vetorInt.length];
        while (numero > 0) {
            int digito = numero % 10;
            vetorDigitos[index] = digito;
            index++;
            numero = numero / 10;
        }
        int[] vetorDigitosAjustado = redimensionaVetor(vetorDigitos, index);
        for (int elemento = 0; elemento < vetorDigitosAjustado.length; elemento++) {
            if (elemento + 1 != vetorDigitosAjustado.length) {
                eIgual = vetorDigitosAjustado[elemento] == vetorDigitosAjustado[elemento + 1];
            }
        }
        return eIgual;
    }

    /**
     * verifica se um vetor é igual a outro vetor
     *
     * @param vetorQualquer
     * @return
     */
    public boolean vetorIgual(int[] vetorQualquer) {
        boolean vetorIgual = Arrays.equals(vetorInt, vetorQualquer);
        return vetorIgual;
    }
}


