import bloco4.Exercicio13;
import bloco4.Exercicio15;

public class ArrayBidimensional extends Array {

    private int[][] matrizInt = new int[0][0];

    public ArrayBidimensional() {
    }

    public ArrayBidimensional(int[][] matriz) {
        this.matrizInt = matriz;
    }

    /**
     * adiciona um elemento na matriz, conforme a linha, mete no fim
     *
     * @param elemento
     * @param linha
     */
    public void adicionarElementoMatriz(int elemento, int linha) {
        int[] vetorTemporario = new int[matrizInt[linha].length + 1];
        int index = 0;
        for (int item : matrizInt[linha]) {
            vetorTemporario[index] = item;
            index++;
        }
        vetorTemporario[vetorTemporario.length] = elemento;
        this.matrizInt[linha] = vetorTemporario;
    }

    /**
     * retira primeira coluna da Matriz
     */
    public void retiraPrimeiraColunaMatriz() {
        int index = 0;
        int posicao = 0;
        int[] vetorTemporario = new int[matrizInt[index].length];
        for (int linha = 0; linha < matrizInt.length; linha++) {
            for (int item : matrizInt[index]) {
                vetorTemporario[index] = item;
                index++;
            }
            retiraPrimeiroElemento(vetorTemporario);
            matrizInt[posicao] = vetorTemporario;
            posicao++;
        }
    }

    /**
     * Verifica se a matriz é vazia
     *
     * @return
     */
    public boolean matrizEVazia() {
        return matrizInt.length == 0;
    }

    /**
     * retorna o maior elemento da matriz
     *
     * @return
     */
    public int maiorElementoMatriz() {
        int index = 0;
        int maiorElementoMatriz = 0;
        for (int linha = 0; linha < matrizInt[index].length; linha++) {
            int[] vetorTemp = new int[matrizInt[linha].length];
            for (int item : matrizInt[linha]) {
                vetorTemp[index] = item;
                index++;
            }
            index = 0;
            int maiorElementoVetorTemp = retornaMaiorElementoVetor(vetorTemp);
            if (maiorElementoVetorTemp > maiorElementoMatriz) {
                maiorElementoMatriz = maiorElementoVetorTemp;
            }
        }
        return maiorElementoMatriz;
    }

    /**
     * retorna menor elemento da matriz
     *
     * @return
     */
    public int menorElementoMatriz() {
        int index = 0;
        int menorElementoMatriz = matrizInt[0][0];
        for (int linha = 0; linha < matrizInt[index].length; linha++) {
            int[] vetorTemp = new int[matrizInt[linha].length];
            for (int item : matrizInt[linha]) {
                vetorTemp[index] = item;
                index++;
            }
            index = 0;
            int menorElementoVetorTemp = retornaMenorElementoVetor(vetorTemp);

            if (menorElementoVetorTemp < menorElementoMatriz) {
                menorElementoMatriz = menorElementoVetorTemp;
            }
        }
        return menorElementoMatriz;
    }

    /**
     * retorna a media dos elementos da matriz
     *
     * @return
     */
    public int mediaElementosMatriz() {
        int index = 0;
        int mediaMatriz;
        int somaMedias = 0;
        for (int linha = 0; linha < matrizInt.length; linha++) {
            int[] vetorTemp = new int[matrizInt[linha].length];
            for (int item : matrizInt[linha]) {
                vetorTemp[index] = item;
                index++;
            }
            index = 0;
            somaMedias += mediaDosElementosDoVetor(vetorTemp);
        }
        if (matrizInt.length != 0) {
            mediaMatriz = somaMedias / matrizInt.length;
        } else {
            mediaMatriz = 0;
        }
        return mediaMatriz;
    }

    /**
     * retorna soma de cada linha da matriz
     *
     * @return
     */
    public int[] retornaSomaDeCadaLinha() {
        int[] vetorSomaDasLinhas = new int[matrizInt.length];
        int index = 0;
        for (int linha = 0; linha < matrizInt.length; linha++) {
            int somaElementos = somaElementosVetor(matrizInt[linha]);
            vetorSomaDasLinhas[index] = somaElementos;
            index++;
        }
        return vetorSomaDasLinhas;
    }

    /**
     * retorna um vetor com a soma de cada coluna
     *
     * @return
     */
    public int[] retornaSomaDeCadaColuna() {
        int tamanhoMaiorLinha = retornaTamanhoDaMaiorLinhaDaMatriz(matrizInt);
        int[] vetorSomaDasColunas = new int[tamanhoMaiorLinha];

        for (int col = 0; col < tamanhoMaiorLinha; col++) {
            int soma = 0;
            for (int linha = 0; linha < matrizInt.length; linha++) {
                if (matrizInt[linha].length > col) {
                    soma += matrizInt[linha][col];
                }
            }
            vetorSomaDasColunas[col] = soma;
        }
        return vetorSomaDasColunas;
    }

    /**
     * retorna o tamanho da maior linha de uma matriz
     *
     * @param matrizInteiros
     * @return
     */
    public int retornaTamanhoDaMaiorLinhaDaMatriz(int[][] matrizInteiros) {
        this.matrizInt = matrizInteiros;
        if (matrizInteiros.length == 0) {
            return 0;
        }
        int maiorLinha = matrizInteiros[0].length;
        for (int linha = 0; linha < matrizInteiros.length; linha++) {
            if (maiorLinha < matrizInteiros[linha].length) {
                maiorLinha = matrizInteiros[linha].length;
            }
        }
        return maiorLinha;
    }

    /**
     * retorna o indice do vetor com maior soma
     *
     * @return
     */
    public int retornaIndiceDoVetorDeMaiorSoma() {
        int[] somaDosVetores = retornaSomaDeCadaLinha();
        int maiorVetor = somaDosVetores[0];
        int indiceMaiorVetor = 0;
        for (int linha = 0; linha < somaDosVetores.length; linha++) {
            if (maiorVetor < somaDosVetores[linha]) {
                maiorVetor = somaDosVetores[linha];
                indiceMaiorVetor = linha;
            }
        }
        return indiceMaiorVetor;
    }

    /**
     * verifica se matriz é quadrada
     *
     * @param matrizInteiros
     * @return
     */
    public boolean verificaSeMatrizEhQuadrada(int[][] matrizInteiros) {
        this.matrizInt = matrizInteiros;
        return Exercicio13.verificaMatrizQuadrada(matrizInteiros);
    }

    /**
     * verifica se a matriz é simétrica
     *
     * @return
     */
    public boolean verificaMatrizSimetrica() {
        int verificadorFalso = 0;
        if (!verificaSeMatrizEhQuadrada(matrizInt)) {
            throw new IllegalArgumentException("Matriz não é quadrada");
        }
        for (int linha = 0; linha < matrizInt.length; linha++) {
            for (int coluna = 0; coluna < matrizInt[linha].length; coluna++) {
                if (matrizInt[linha][coluna] != matrizInt[coluna][linha]) {
                    verificadorFalso++;
                }
            }
        }
        if (verificadorFalso > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * retorna quantidade de elementos na diagonal principal da matriz, quando for quadrada
     *
     * @return
     */
    public int quantElementosDiagonalPrincipal() {
        if (verificaSeMatrizEhQuadrada(matrizInt)) {
            return retornaNumeroElementosVetor(Exercicio15.diagonalPrincipal(matrizInt));
        } else {
            return -1;
        }
    }

    /**
     * verifica se o conteúdo das diagonais principal e secundária são iguais
     *
     * @return
     */
    public boolean diagonaisIguais() {
        if (matrizInt == null || matrizInt.length == 0) {
            return false;
        }
        int[] diagonalPrincipal = Exercicio15.diagonalPrincipal(matrizInt);
        int[] diagonalSecundária = Exercicio15.diagonalSecundaria(matrizInt);
        for (int posicao = 0; posicao < matrizInt.length; posicao++) {
            if (diagonalPrincipal[posicao] != diagonalSecundária[posicao]) {
                return false;
            }
        }
        return true;

    }

    /**
     * calcula media de algarismos de uma matriz
     *
     * @return
     */
    public int mediaAlgarismosMatriz() {
        int quantAlgarismosMatriz = 0;
        for (int linha = 0; linha < matrizInt.length; linha++) {
            quantAlgarismosMatriz += mediaAlgarismosDoVetor(matrizInt[linha]);
        }
        return quantAlgarismosMatriz / matrizInt.length;
    }

    /**
     * retorna os elementos com algarismos acima da média dos algarismos da matriz
     *
     * @return
     */
    public int[] algarismosAcimaDaMediaMatriz() {
        int quantAcimadaMedia = 0;
        int index = 0;
        int[] algarismosAcimaDaMedia = new int[matrizInt.length * retornaTamanhoDaMaiorLinhaDaMatriz(matrizInt)];
        for (int linha = 0; linha < matrizInt.length; linha++) {
            for (int coluna = 0; coluna < matrizInt[linha].length; coluna++) {
                int quantAlgarismos = numeroDeDigitosInt(matrizInt[linha][coluna]);
                if (quantAlgarismos > mediaAlgarismosMatriz()) {
                    algarismosAcimaDaMedia[index] = matrizInt[linha][coluna];
                    quantAcimadaMedia++;
                    index++;
                }
            }
        }
        return redimensionaVetor(algarismosAcimaDaMedia, quantAcimadaMedia);
    }

    /**
     * retorna a percentagem de pares na matriz
     *
     * @return
     */
    public int percentagemDeParesNaMatriz(int[][] matrizInteiros) {
        this.matrizInt = matrizInteiros;
        int totalAlgarismos = 0;
        int somaPercentagens = 0;
        for (int linha = 0; linha < matrizInt.length; linha++) {
            somaPercentagens += percentagemAlgarismosParesVetor(matrizInt[linha]);
            totalAlgarismos += totalAlgarismosVetor(matrizInt[linha]);
        }
        return somaPercentagens / matrizInt.length;
    }

    /**
     * retorna os elementos mais maior percentagem de pares com relação a percentagem da matriz
     *
     * @return
     */
    public int[] elementosComMaiorPercentagemParesMatriz() {
        int[] elementosComMaisPares = new int[matrizInt.length * retornaTamanhoDaMaiorLinhaDaMatriz(matrizInt)];
        int index = 0;
        for (int linha = 0; linha < matrizInt.length; linha++) {
            for (int coluna = 0; coluna < matrizInt[linha].length; coluna++) {
                if (percentagemParesNumInteiro(matrizInt[linha][coluna]) > percentagemDeParesNaMatriz(matrizInt)) {
                    elementosComMaisPares[index] = matrizInt[linha][coluna];
                    index++;
                }
            }
        }
        return redimensionaVetor(elementosComMaisPares, index);
    }

    /**
     * percentagem de pares num inteiro
     *
     * @param numero
     * @return
     */
    public int percentagemParesNumInteiro(int numero) {
        int quantDePares = 0;
        int elemento = numero;
        if (numero == 0) {
            quantDePares++;
        }
        while (elemento > 0) {
            int digito = elemento % 10;
            if (digito % 2 == 0) {
                quantDePares++;
            }
            elemento /= 10;
        }
        return quantDePares * 100 / numeroDeDigitosInt(numero);
    }

    /**
     * inverte a ordem dos elementos de cada linha da matriz
     *
     * @return
     */
    public int[][] inverterOrdemDasLinhas() {
        int[][] matrizInvertida = new int[matrizInt.length][];
        int i = 0;
        for (int linha = 0; linha < matrizInt.length; linha++) {
            matrizInvertida[linha] = new int[matrizInt[linha].length];
            for (int coluna = 0; coluna < matrizInt[linha].length; coluna++) {
                matrizInvertida[linha][coluna] = matrizInt[linha][(matrizInt[linha].length - 1) - i];
                i++;
            }
            i = 0;
        }
        return matrizInvertida;
    }

    /**
     * inverte a ordem dos elementos das colunas da matriz
     * @return
     */
    public int[][] inverterOrdemDasColunas() {
        int[][] matrizColunasIvertidas = new int[matrizInt.length][];
        for (int linha = 0; linha < matrizInt.length; linha++) {
            matrizColunasIvertidas[linha] = new int[(matrizInt.length - 1) - linha];
            matrizColunasIvertidas[linha] = matrizInt[(matrizInt.length - 1) - linha];
        }
        return matrizColunasIvertidas;
    }

}