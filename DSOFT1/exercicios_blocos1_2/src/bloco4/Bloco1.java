package bloco4;

public class Bloco1 {

    public static double exercicio3_V2(double altura, double raio) {
        double volume;
        volume = (3.14 * Math.pow(raio, 2) * altura) * 1000; //m³->l
        return volume;
    }

    public static double exercicio4_V2(double intervaloSegundos) {
        double velocidadeSom = 1224 / 3.6;      // km/h -> m/s
        double distancia = velocidadeSom * intervaloSegundos;
        return distancia;
    }

    public static double exercicio5_V2(double tempo) {
        double altura, velocidadeInicial, aceleracao;
        velocidadeInicial = 0;
        aceleracao = 9.8;
        altura = (velocidadeInicial * tempo) + (aceleracao * tempo * tempo) / 2;
        return altura;
    }

    public static double exercicio6_V2(double sombraEdificio, double sombraPessoa, double alturaPessoa) {
        double alturaEdificio;
        alturaEdificio = Math.round(alturaPessoa * (sombraEdificio / sombraPessoa));
        return alturaEdificio;
    }
    public static double exercicio7_V2() {
        double distanciaManel, tempoManel, tempoZe, distanciaZe;
        distanciaManel = 42195;
        tempoManel = 4 * 3600 + 2 * 60 + 10; //min->sec
        tempoZe = 1 * 3600 + 5 * 60;
        distanciaZe = Math.round(tempoZe * (distanciaManel / tempoManel));
        return distanciaZe;
    }

    public static double exercicio8_V2(double a, double b) {
        double c;
        double anguloY = 60;
        //c²=a²+ b² -2abcosY
        c = Math.floor(Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) - (2 * a * b * Math.cos(anguloY * (Math.PI / 180)))));
        return c;
    }

    public static double exercicio9_V2(double a, double b) {
        double perimeter = (2 * a) + (2 * b);
        return perimeter;
    }
    public static double exercicio10_V2(double c1, double c2){
        double h = Math.sqrt(Math.pow(c1, 2) + Math.pow(c2, 2));
        return Math.round(h * 100d) / 100d;
    }
    public static double exercicio11_V2(double x){
        double result;
        result = Math.pow(x,2) - ((3*x) +1);
        return result;
    }
    public static double exercicio12_V2(double celsius) {
        double fahreinheit;
        fahreinheit = 32 +(9/5)* celsius;
        return fahreinheit;
    }
    public static int exercicio13(int h, int m){
        int totalMin = h*60 + m;
        return totalMin;
    }
}
