package bloco4;

public class Bloco2 {

    /*public static void exercicio1c() {
        int nota1, nota2, nota3, peso1, peso2, peso3;
        double mediaPesada;
        Scanner read = new Scanner(System.in);
        System.out.println("Insira a nota1 do aluno");
        nota1 = read.nextInt();
        System.out.println("Insira o peso da prova1");
        peso1 = read.nextInt();
        System.out.println("Insira a nota2 do aluno");
        nota2 = read.nextInt();
        System.out.println("Insira o peso da prova2");
        peso2 = read.nextInt();
        System.out.println("Insira a nota3 do aluno");
        nota3 = read.nextInt();
        System.out.println("Insira o peso da prova3");
        peso3 = read.nextInt();
        mediaPesada = getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);
    }
    public static double getMediaPesada(int nota1, int nota2, int nota3, int peso1, int peso2, int peso3) {
        double mediaPesada = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);
        System.out.println("A nota do aluno é " + mediaPesada);
        if (mediaPesada >= 8) {
            System.out.println("Este aluno cumpre a nota mínima exigida");
            return mediaPesada;
        } else {
            System.out.println("Este aluno NÂO cumpre a nota mínima exigida");
        }
        return mediaPesada;
    }*/
    public static String exercicio1c_V2(int nota1, int nota2, int nota3, int peso1, int peso2, int peso3) {
        double mediaPesada = ((nota1 * peso1) + (nota2 * peso2) + (nota3 * peso3)) / (peso1 + peso2 + peso3);
        if (mediaPesada >= 8) {
            return ("Este aluno cumpre a nota mínima exigida");
        } else {
            return ("Este aluno NÂO cumpre a nota mínima exigida");
        }
    }

    /*public static void exercicio2c() {
        int numero, digito1, digito2, digito3;

        Scanner read = new Scanner(System.in);
        System.out.println("Digite uma senha com 3 números");
        numero = read.nextInt();
        getDigits(numero);
    }*/
    public static String exercicio2c(int numero) {
        String result;
        if (numero < 100 || numero > 999) {
            return "Número não tem 3 digitos";
        } else {
            int digito3 = numero % 10;
            int digito2 = (numero / 10) % 10;
            int digito1 = (numero / 100) % 10;
            result = digito1 + " " + digito2 + " " + digito3;
            return result;
        }
    }
    /*public static void getDigits(int numero) {
        if (numero < 100 || numero > 999) {
            System.out.println("Número não tem 3 digitos");
        } else {
            int digito3 = numero % 10;
            int digito2 = (numero / 10) % 10;
            int digito1 = (numero / 100) % 10;
            System.out.println(digito1 + " " + digito2 + " " + digito3);
        }
    }*/

    /*public static void exercicio3() {
        int x1, x2, y1, y2;
        double d;
        Scanner read = new Scanner(System.in);
        System.out.println("Digite o valor de P no ponto x1");
        x1 = read.nextInt();
        System.out.println("Digite o valor de P no ponto x2");
        x2 = read.nextInt();
        System.out.println("Digite o valor de P no ponto y1");
        y1 = read.nextInt();
        System.out.println("Digite o valor de P no ponto y1");
        y2 = read.nextInt();
        d = getSqrt(x1, x2, y1, y2);
    }

    public static double getSqrt(int x1, int x2, int y1, int y2) {
        double d = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
        System.out.println("A distancia entre os pontos é " + Math.round(d * 100d) / 100d + " unidades");
        return Math.round(d * 100d) / 100d;
    }*/
    public static double exercicio3(int x1, int x2, int y1, int y2) {
        double d = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
        return Math.round(d * 100d) / 100d;
    }

    public static double exercicio4(double x) {
        if (x < 0) {
            return x;
        } else if (x == 0) {
            return 0;
        } else {
            return (Math.pow(x, 2) - 2 * x);
        }
    }

    /*public static void exercicio5() {
        double area;
        Scanner read = new Scanner(System.in);
        System.out.println("Digite o valor da area");
        area = (read.nextDouble()) / 100; //passei para dm²;
        getVolume(area);
    }

    public static void getVolume(double area) {
        if (area > 0) {
            double aresta = Math.sqrt(area / 6);
            double volume = Math.round(Math.pow(aresta, 3)) * 100d / 100d;
            getClassification(volume);
            System.out.println("Volume do cubo= " + volume + " dm³");
        } else {
            System.out.println("Valor da área incorreto");
        }
    }*/
    public static String exercicio5c(double area){
        area = area / 100; //passei a área de cm² para dm²
        double aresta = Math.sqrt(area / 6);
        double volume = getVolume(aresta);
        String classification = getClassification(volume);
        String resultado = "Volume do cubo = " + volume + " dm³";
        if (area > 0) {
            return resultado;
        } else {
            return ("Valor da área incorreto");
        }
    }

    public static double getVolume(double aresta) {
        return Math.round(Math.pow(aresta, 3)) * 100d / 100d;
        //só importa que o volume tenha 2 casas decimais
    }

    public static String getClassification(double volume) {
        if (volume <= 1) {
            return "Classificação: Pequeno";
        } else if (volume > 1 && volume <= 2) {
            return "Classificação: Médio";
        } else {
            return "Classificação: Grande";
        }
    }

    public static String exercicio6(double segundosTotais) {
        double horas = segundosTotais / 3600;
        double horasParteDecimal = horas % 1;
        double horasParteInteira = horas - horasParteDecimal;

        double minutos = (horasParteDecimal) * 60;
        double minutosParteDecimal = minutos % 1;
        double minutosParteInteira = minutos - minutosParteDecimal;

        double segundosRestantes = minutosParteDecimal * 60;
        double segundoRestantesParteDecimal = segundosRestantes % 1;
        double segundosRestantesParteInteira = segundosRestantes - segundoRestantesParteDecimal;

        String horario = horasParteInteira + ":" + minutosParteInteira + ":" + segundosRestantesParteInteira;

        System.out.println(horario);//apenas para não ter uma variável ainda ñ utilizada
        if (horas >= 6 && horas <= 12) {
            return "Bom dia";
        } else if (horas > 12 && horas <= 20) {
            return "Boa tarde";
        } else {
            return "Boa noite";
        }
    }

    public static double exercicio7(double area, double custoLitroTinta, double salarioPintor, double rendimentoTinta) {
        double custoTotal;
        /*int pintores;
        if (area <= 100) {
            pintores = 1;
        } else if (area > 100 && area <= 300) {
            pintores = 2;
        } else if (area > 300 && area <= 1000) {
            pintores = 3;
        } else {
            pintores = 4;
        }*/
        double horasTotaisDeTrabalho = 4 * area;
        double custoMaoDeObra = salarioPintor * horasTotaisDeTrabalho;

        double custoTotalTinta = (rendimentoTinta * area) * custoLitroTinta;

        custoTotal = custoMaoDeObra + custoTotalTinta;

        return custoTotal;
    }

    public static String exercicio8(int x, int y) {
        int z = 2;
        if (x == z * y) {
            return "x é multiplo de y";
        } else if (y == z * x) {
            return "y é multiplo de x";
        } else {
            return "X não é multiplo nem divisor de y";
        }
    }

    public static String exercicio9(int numero) {
        int primeiroDigito = (numero / 100) % 10;
        int segundoDigito = (numero / 10) % 10;
        int terceiroDigito = numero % 10;

        if (segundoDigito == primeiroDigito + 1 && terceiroDigito == segundoDigito + 1) {
            return "Este número tem sequencia crescente";
        } else {
            return "Este número não tem sequencia crescente";
        }
    }

    public static double exercicio10_calculaPreco(double preco) {
        double novoPreco;
        if (preco > 200) {
            novoPreco = preco - (0.6 * preco);
            return novoPreco;
        } else if (preco > 100 && preco <= 200) {
            novoPreco = preco - (0.4 * preco);
            return novoPreco;
        } else if (preco > 50 && preco <= 100) {
            novoPreco = preco - (0.3 * preco);
            return novoPreco;
        } else {
            novoPreco = preco - (0.2 * preco);
            return novoPreco;
        }

    }

    public static String exercicio11_percentagemAprovados(double aprovados) {
        if (aprovados < 0 || aprovados > 1) {
            return "Valor inválido";
        } else {
            if (aprovados < 0.2) {
                return "Turma má";
            } else if (aprovados < 0.5) {
                return "Turma fraca";
            } else if (aprovados < 0.7) {
                return "Turma razoável";
            } else if (aprovados < 0.9) {
                return "Turma boa";
            } else {
                return "Turma excelente";
            }
        }
    }

    public static String exercicio12_indicePoluentes(double indicePoluicao) {
        if (indicePoluicao > 0.3 && indicePoluicao <= 0.4) {
            return "Suspender atividades do Grupo 1";
        } else if (indicePoluicao > 0.4 && indicePoluicao <= 0.5) {
            return "Suspender atividades dos Grupos 1 e 2";
        } else if (indicePoluicao > 0.5) {
            return "Suspender atividades dos Grupos 1, 2 e 3";
        } else {
            return "Indice de poluição aceitável";
        }
    }

    public static double exercicio13_custosJardim(int quantidadeArvores, int quantidadeArbustos, int metrosDeGrama) {
        double horasDeServico = ((quantidadeArbustos * 400) + (quantidadeArvores * 600) + (metrosDeGrama * 300)) / 3600;//passei de segundos para horas
        double custoTrabalho = Math.round(horasDeServico * 10);//158,33
        double precoArbusto = 15;
        double precoArvore = 20;
        double precoGrama = 10;
        double custoJardim = (quantidadeArbustos * precoArbusto) + (quantidadeArvores * precoArvore) + (metrosDeGrama * precoGrama);
        double custoProjetoFinal;
        custoProjetoFinal = custoTrabalho + custoJardim;
        return custoProjetoFinal;
    }

    public static double exercicio14_distanciaMediaDiaria(double distanciaDia1, double distanciaDia2, double distanciaDia3, double distanciaDia4, double distanciaDia5) {
        double somaDistancias = (distanciaDia1 + distanciaDia2 + distanciaDia3 + distanciaDia4 + distanciaDia5) * 1609;
        return (double) Math.round((somaDistancias / 5) / 1000);
    }

    public static String exercicio15_tiposTriangulos(int a, int b, int c) {
        if (a <= 0 && b <= 0 && c <= 0) {
            return "Este não pode ser um triangulo";
        } else if (a > c + b || c > a + b || b > a + c) {
            return "Este não pode ser um triangulo";
        } else {
            if (a == b && a == c) {
                return "Este é um triangulo equilátero";
            } else if (a != b && b != c && c != a) {
                return "Este é um triangulo escaleno";
            } else {
                return "Este é um triangulo retangulo";
            }
        }
    }

    public static String exercicio16_angulosTriangulos(int a, int b, int c) {
        if (a <= 0 && b <= 0 && c <= 0) {
            return "Este não pode ser um triangulo";
        } else if (a + b + c != 180) {
            return "Este não pode ser um triangulo";
        } else {
            if (a == 90 || b == 90 || c == 90) {
                return "Este é um triangulo retangulo";
            } else if (a > 90 || b > 90 || c > 90) {
                return "Este é um triangulo obtusangulo";
            } else {
                return "Este é um triangulo acutangulo";
            }
        }
    }

    public static String exercicio17_chegadaViagem(int horaSaida, int minutoSaida, int duracaoHora, int duracaoMinuto) {
        int horaChegada = horaSaida + duracaoHora;
        int minutosChegada = minutoSaida + duracaoMinuto;
        String horarioChegada;

        if (minutosChegada >= 60) {
            minutosChegada = minutosChegada - 60;
            horaChegada = horaChegada + 1;
        }

        if (horaChegada > 24) {
            horaChegada = horaChegada - 24;
            horarioChegada = horaChegada + ":" + minutosChegada;
            if (minutosChegada < 10) {
                horarioChegada = horaChegada + ":0" + minutosChegada;
            }
            return "Chegará às " + horarioChegada + " do dia seguinte";
        } else {
            horarioChegada = horaChegada + ":" + minutosChegada;
            if (minutosChegada < 10) {
                horarioChegada = horaChegada + ":0" + minutosChegada;
            }
            return "Chegará às " + horarioChegada + " do mesmo dia.";
        }

    }

    public static String exercicio18_tempoFimProcessamento(int horaInicio, int minutoInicio, int segundoInicio, int tempoEmSegundosProcessamento) {
        int tempoInicial = horaInicio * 3600 + minutoInicio * 60 + segundoInicio; //passa tudo pra segundos
        int tempoFim = tempoInicial + tempoEmSegundosProcessamento;
        int horaFim = tempoFim / 3600;
        int minutoFim = (tempoFim % 3600) / 60;
        int segundoFim = (tempoFim / 3600) % 60;
        return horaFim + ":" + minutoFim + ":" + segundoFim;
    }

    public static double exercicio19_calcSalario(int horasExtras) {
        int horasFixas = 36;
        double salarioHora = 7.5;
        double salarioHoraExtra;
        double salarioSemanal;
        if (horasExtras <= 5) {
            salarioHoraExtra = 10;
            salarioSemanal = (horasFixas * salarioHora) + (horasExtras * salarioHoraExtra);
        } else {
            salarioHoraExtra = 15;
            salarioSemanal = (horasFixas * salarioHora) + ((horasExtras - 5) * salarioHoraExtra) + 50;
        }

        return salarioSemanal;
    }

    public static double exercicio20_aluguerKitJardinagem(String kitOptions, int diaSemana,double distanciaCliente) {
        double preco;
        if (diaSemana >= 2 && diaSemana < 6) { //quando comparo Strings é correto usar == ou usar nomedavariável.equals()?
            if (kitOptions.equals("a")|| kitOptions.equals("A")) {
                preco = 30;
            } else if (kitOptions.equals("b") || kitOptions.equals("B")) {
                preco = 50;
            } else {
                preco = 100;
            }
        } else {
            if (kitOptions.equals("a")|| kitOptions.equals("A")) {
                preco = 40;
            } else if (kitOptions.equals("b") || kitOptions.equals("B")) {
                preco = 70;
            } else {
                preco = 140;
            }
        }
        return preco + (2*distanciaCliente);

    }

}