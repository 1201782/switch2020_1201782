package bloco4;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //exercicio3();
        //exercicio4();
        //exercicio5();
        //exercicio6();
        //exercicio7();
        //exercicio8();
        //exercicio9();
        //exercicio10();
        //exercicio11();
        //exercicio12();
        //exercicio13();
    }

    public static void exercicio3() {

        //Dados
        double raio, altura, volume;

        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o valor do raio da base em metros");
        raio = ler.nextDouble();
        System.out.println("Introduza o valor da altura em metros");
        altura = ler.nextDouble();

        //Processamento recorrendo ao metodo volume do cilindro
        volume = obterVolume(raio, altura) * 1000;

        //saida de dados;
        System.out.println("O volume em litro é de " + String.format("%.2f", volume) + " litros");

    }

    public static double obterVolume(double raio, double altura) {
        double volume;
        //volume cilindro
        volume = Math.PI * raio * raio * altura;
        return volume;
    }

    public static void exercicio4() {
        //Dados
        double velocidadeSom, intervaloSegundos, distancia;

        //Leitura
        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza tempo entre a trovoada e o relampago");
        intervaloSegundos = ler.nextDouble();

        //Processamento
        velocidadeSom = 1224 / 3.6;
        distancia = pegarDistancia(velocidadeSom, intervaloSegundos);
        //saida de dados
        System.out.println("A distância entre o trovão e o relampago é de " + String.format("%.2f", distancia) + " metros"); //procurar como formatar numeros
    }

    public static double pegarDistancia(double velocidadeSom, double intervaloSegundos) {
        double distancia;
        distancia = velocidadeSom * intervaloSegundos;
        return distancia;
    }

    public static void exercicio5() {
        //Dados
        double altura, velocidadeInicial, aceleracao, tempo;

        //Leitura de dados

        Scanner ler = new Scanner(System.in);
        System.out.println("Quanto tempo o objeto levou para chegar ao solo?");
        tempo = ler.nextDouble();

        //Processamento
        velocidadeInicial = 0;
        aceleracao = 9.8;
        altura = pegarAltura(velocidadeInicial, aceleracao, tempo);

        //Saida de dados
        System.out.println("A altura do prédio é de " + String.format("%.2f", altura) + " metros");
    }

    public static double pegarAltura(double velocidadeInicial, double aceleracao, double tempo) {
        return (velocidadeInicial * tempo) + (aceleracao * tempo * tempo) / 2;
    }

    public static void exercicio6() {
        //Dados
        double alturaEdificio, sombraEdificio, alturaPessoa, sombraPessoa;

        //Leitura
        Scanner ler = new Scanner(System.in);
        System.out.println("Digite quanto mede a sombra do edificio");
        sombraEdificio = ler.nextDouble();
        System.out.println("Digite quanto a pessoa tem de altura");
        alturaPessoa = ler.nextDouble();
        System.out.println("Dite quanto mede a sombra da pessoa");
        sombraPessoa = ler.nextDouble();

        //Processamento
        alturaEdificio = calcularAlturaEdificio(sombraEdificio, alturaPessoa, sombraPessoa);
        //Saida
        System.out.println("A altura do prédio é de " + String.format("%.2f", alturaEdificio) + " metros");

    }

    public static double calcularAlturaEdificio(double sombraEdificio, double alturaPessoa, double sombraPessoa) {
        return alturaPessoa * (sombraEdificio / sombraPessoa);
    }

    public static void exercicio7() {
        //Dados
        float distanciaZe, distanciaManel, tempoManel, tempoZe;
        //Leitura de dados
        Scanner ler = new Scanner(System.in);
        System.out.println("Quantos metros percorreu Manel?");
        distanciaManel = ler.nextFloat();
        System.out.println("Em quanto tempo (em segundos) o Manel fez a prova?");
        tempoManel = ler.nextFloat();
        System.out.println("Em quanto tempo (em segundos) o fé fez a prova?");
        tempoZe = ler.nextFloat();
        //Processamento
        distanciaZe = calcularDistanciaZe(distanciaManel, tempoManel, tempoZe);
        //Saida de dados
        System.out.println("A distancia que o Zé percorreu foi de " + String.format("%.2f", distanciaZe) + " metros");
    }

    public static float calcularDistanciaZe(float distanciaManel, float tempoManel, float tempoZe) {
        return tempoZe * (distanciaManel / tempoManel);
    }

    public static void exercicio8() {
        //Dados
        double a, b, c, anguloY;
        //Ler
        Scanner ler = new Scanner(System.in);
        System.out.println("Qual o comprimento entre os pontos AC?");
        b = ler.nextDouble();
        System.out.println("Qual o comprimento entre os pontos CB?");
        a = ler.nextDouble();
        System.out.println("Qual a medida em graus do angulo Y?");
        anguloY = ler.nextDouble();

        //Processar dados
        //c²=a²+ b² -2abcosY
        c = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) - (2 * a * b * Math.cos(anguloY * (Math.PI / 180))));
        //Saida de dados
        System.out.println("A distancia entre os dois homens é de " + String.format("%.2f", c) + " metros");
    }

    public static void exercicio9() {
        //Dados
        double a, b, perimeter;

        //Leitura
        Scanner read = new Scanner(System.in);
        System.out.println("Write A");
        a = read.nextDouble();
        System.out.println("Write B");
        b = read.nextDouble();

        //Processamento
        perimeter = getPerimeter(a, b);

        //Saida
        System.out.println("The perimeter is: " + perimeter);
    }

    public static double getPerimeter(double a, double b) {
        return (2 * a) + (2 * b);
    }

    public static void exercicio10() {
        double c1, c2, h;

        Scanner read = new Scanner(System.in);
        System.out.println("C1 is:");
        c1 = read.nextDouble();
        System.out.println("C2 is:");
        c2 = read.nextDouble();

        h = getSqrt(c1, c2);
        DecimalFormat fmt = new DecimalFormat("0.00");
        System.out.println("The hypotenuse is " + fmt.format(h));
    }

    public static double getSqrt(double c1, double c2) {

        return Math.sqrt(Math.pow(c1, 2) + Math.pow(c2, 2));
    }

    public static void exercicio11() {
        double x, result;
        Scanner read = new Scanner(System.in);
        x = read.nextDouble();
        result = Math.pow(x, 2) - (3 * x) + 1;
        System.out.println("O resultado da expressão é " + result);
    }

    public static void exercicio12() {
        double celsius,fahreinheit;
        Scanner read = new Scanner(System.in);
        celsius = read.nextDouble();
        fahreinheit = 32 +(9/5)* celsius;
        System.out.println("A temperatura em Fahreinheit é de: " + fahreinheit +"º");
    }

    public static void exercicio13(){
        int h, m;
        Scanner read = new Scanner(System.in);
        System.out.println("Escreva as horas(somente números no formato: 0-24)");
        h = read.nextInt();
        System.out.println("Escreva os minutos(somente números)");
        m = read.nextInt();
        int totalMin = h*60 + m;
        System.out.println("O número total de minutos é " + totalMin);
    }
}
