package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Bloco2Test {

    @Test
    public void exercicio1c_V2_Test1() {
        int nota1 = 10;
        int nota2 = 10;
        int nota3 = 10;
        int peso1 = 1;
        int peso2 = 2;
        int peso3 = 3;

        String expected = "Este aluno cumpre a nota mínima exigida";

        String result = Bloco2.exercicio1c_V2(nota1, nota2, nota3, peso1, peso2,peso3);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio1c_V2_Test2() {
        int nota1 = 8;
        int nota2 = 10;
        int nota3 = 5;
        int peso1 = 1;
        int peso2 = 2;
        int peso3 = 3;

        String expected = "Este aluno NÂO cumpre a nota mínima exigida";

        String result = Bloco2.exercicio1c_V2(nota1, nota2,nota3, peso1, peso2,peso3);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio1c_V2_Test3() {
        int nota1 = 7;
        int nota2 = 5;
        int nota3 = 5;
        int peso1 = 1;
        int peso2 = 2;
        int peso3 = 3;

        String expected = "Este aluno NÂO cumpre a nota mínima exigida";

        String result = Bloco2.exercicio1c_V2(nota1, nota2,nota3, peso1, peso2,peso3);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void exercicio2c_Test1() {
        int numero = 333;
        String expected = "3 3 3";
        String result = Bloco2.exercicio2c(numero);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio2c_Test2() {
        int numero = 01;
        String expected = "Número não tem 3 digitos";
        String result = Bloco2.exercicio2c(numero);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio2c_Test3() {
        int numero = 000;
        String expected = "Número não tem 3 digitos";
        String result = Bloco2.exercicio2c(numero);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio2c_Test4() {
        int numero = 0001;
        String expected = "Número não tem 3 digitos";
        String result = Bloco2.exercicio2c(numero);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio2c_Test5() {
        int numero = 001;
        String expected = "Número não tem 3 digitos";
        String result = Bloco2.exercicio2c(numero);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio2c_Test6() {
        int numero = 011;
        String expected = "Número não tem 3 digitos";
        String result = Bloco2.exercicio2c(numero);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio3_Test1() {
        int x1= 1;
        int x2= 1;
        int y1= 1;
        int y2= 1;
        double expected = 0;
        double result = Bloco2.exercicio3(x1,x2,y1,y2);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio3_Test2() {
        int x1= 10;
        int x2= 1;
        int y1= 0;
        int y2= -1;
        double expected = 9.06;
        double result = Bloco2.exercicio3(x1,x2,y1,y2);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio3_Test3() {
        int x1= -990;
        int x2= -1;
        int y1= -90;
        int y2= -15;
        double expected = 991.84;
        double result = Bloco2.exercicio3(x1,x2,y1,y2);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio4_Test1() {
        double x = -5;
        double expected = -5;
        double result = Bloco2.exercicio4(x);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio4_Test2() {
        double x = 5;
        double expected = 15;
        double result = Bloco2.exercicio4(x);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio4_Test3() {
        double x = 0;
        double expected = 0;
        double result = Bloco2.exercicio4(x);
        Assert.assertEquals(expected,result,0.001);
    }

    @Test
    public void exercicio8_Test1() {
        int x = 3;
        int y = 5;
        String expected = "X não é multiplo nem divisor de y";
        String result=Bloco2.exercicio8(x,y);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio8_Test2() {
        int x = 3;
        int y = 6;
        String expected = "y é multiplo de x";
        String result=Bloco2.exercicio8(x,y);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio8_Test3() {
        int x = -3;
        int y = -6;
        String expected = "y é multiplo de x";
        String result=Bloco2.exercicio8(x,y);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio8_Test4() {
        int x = 10;
        int y = 5 ;
        String expected = "x é multiplo de y";
        String result=Bloco2.exercicio8(x,y);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void getVolume() {
        double expected = 1;
        double result= Bloco2.getVolume(1);
        Assert.assertEquals(expected,result,0.001);
    }

    @Test
    public void exercicio5c_Test1() {
        double area = 600;
        String expected = "Volume do cubo = 1.0 dm³";
        String result= Bloco2.exercicio5c(area);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio5c_Test2() {
        double area = -10;
        String expected = "Valor da área incorreto";
        String result= Bloco2.exercicio5c(area);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void getClassification_Test1() {
        double volume = 1;
        String expected = "Classificação: Pequeno";
        String result= Bloco2.getClassification(volume);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void getClassification_Test2() {
        double volume = 2;
        String expected = "Classificação: Médio";
        String result= Bloco2.getClassification(volume);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void getClassification_Test3() {
        double volume = 12;
        String expected = "Classificação: Grande";
        String result= Bloco2.getClassification(volume);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void exercicio7() {
        double expected=14800;
        double result=Bloco2.exercicio7(100,10,12,10);
        Assert.assertEquals(expected,result,0.001);
    }

    @Test
    public void exercicio6_Test1() {
        String expected = "Boa tarde";
        String result = Bloco2.exercicio6(50700);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio6_Test2() {
        String expected = "Boa noite";
        String result = Bloco2.exercicio6(10700);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio6_Test3() {
        String expected = "Bom dia";
        String result = Bloco2.exercicio6(25700);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio9_Test1() {
        String expected = "Este número tem sequencia crescente";
        String result = Bloco2.exercicio9(234);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio9_Test2() {
        String expected = "Este número não tem sequencia crescente";
        String result = Bloco2.exercicio9(255);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio10_calculaPreco_Test1() {
        double expected =70;
        double result = Bloco2.exercicio10_calculaPreco(100);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio10_calculaPreco_Test2() {
        double expected =8;
        double result = Bloco2.exercicio10_calculaPreco(10);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio10_calculaPreco_Test3() {
        double expected =400;
        double result = Bloco2.exercicio10_calculaPreco(1000);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio10_calculaPreco_Test4() {
        double expected = 20;
        double result = Bloco2.exercicio10_calculaPreco(25);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio11_Test1() {
        String expected = "Turma excelente";
        String result = Bloco2.exercicio11_percentagemAprovados(1);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio11_Test2() {
        String expected = "Turma má";
        String result = Bloco2.exercicio11_percentagemAprovados(0.1);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio11_Test3() {
        String expected = "Turma fraca";
        String result = Bloco2.exercicio11_percentagemAprovados(0.3);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio11_Test4() {
        String expected = "Turma razoável";
        String result = Bloco2.exercicio11_percentagemAprovados(0.6);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio11_Test5() {
        String expected = "Turma boa";
        String result = Bloco2.exercicio11_percentagemAprovados(0.8);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio12_indicePoluentes_Test1() {
        String expected = "Suspender atividades dos Grupos 1, 2 e 3";
        String result = Bloco2.exercicio12_indicePoluentes(0.8);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio12_indicePoluentes_Test2() {
        String expected = "Suspender atividades dos Grupos 1 e 2";
        String result = Bloco2.exercicio12_indicePoluentes(0.49);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio12_indicePoluentes_Test3() {
        String expected = "Suspender atividades do Grupo 1";
        String result = Bloco2.exercicio12_indicePoluentes(0.356);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio12_indicePoluentes_Test4() {
        String expected = "Indice de poluição aceitável";
        String result = Bloco2.exercicio12_indicePoluentes(0.1);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio13_custosJardim() {
        double expected =2150;
        double result = Bloco2.exercicio13_custosJardim(10,60,90);
        Assert.assertEquals(expected,result,0.001);
    }

    @Test
    public void exercicio14_distanciaMediaDiaria() {
        double expected =59;
        double result = Bloco2.exercicio14_distanciaMediaDiaria(20,30,60,50,22);
        Assert.assertEquals(expected,result,0.001);
    }

    @Test
    public void exercicio15_tiposTriangulos_Test1() {
        String expected = "Este é um triangulo retangulo";
        String result = Bloco2.exercicio15_tiposTriangulos(60,60,30);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void exercicio15_tiposTriangulos_Test2() {
        String expected = "Este é um triangulo escaleno";
        String result = Bloco2.exercicio15_tiposTriangulos(120,80,90);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio15_tiposTriangulos_Test3() {
        String expected = "Este é um triangulo equilátero";
        String result = Bloco2.exercicio15_tiposTriangulos(120,120,120);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio15_tiposTriangulos_Test4() {
        String expected = "Este não pode ser um triangulo";
        String result = Bloco2.exercicio15_tiposTriangulos(-120,120,120);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio15_tiposTriangulos_Test5() {
        String expected = "Este não pode ser um triangulo";
        String result = Bloco2.exercicio15_tiposTriangulos(12128,120,1);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void exercicio16_angulosTriangulos_Test1() {
        String expected = "Este é um triangulo retangulo";
        String result = Bloco2.exercicio16_angulosTriangulos(90,45,45);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio16_angulosTriangulos_Test2() {
        String expected = "Este é um triangulo obtusangulo";
        String result = Bloco2.exercicio16_angulosTriangulos(100,40,40);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio16_angulosTriangulos_Test3() {
        String expected = "Este não pode ser um triangulo";
        String result = Bloco2.exercicio16_angulosTriangulos(60,65,65);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio16_angulosTriangulos_Test4() {
        String expected = "Este é um triangulo acutangulo";
        String result = Bloco2.exercicio16_angulosTriangulos(60,60,60);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio16_angulosTriangulos_Test5() {
        String expected = "Este não pode ser um triangulo";
        String result = Bloco2.exercicio16_angulosTriangulos(60,60,0);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void exercicio17_chegadaViagem_Test1() {
        String expected = "Chegará às 3:09 do dia seguinte";
        String result = Bloco2.exercicio17_chegadaViagem(23,10,3,59);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio17_chegadaViagem_Test2() {
        String expected = "Chegará às 17:00 do mesmo dia.";
        String result = Bloco2.exercicio17_chegadaViagem(15,1,1,59);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio17_chegadaViagem_Test3() {
        String expected = "Chegará às 5:25 do dia seguinte";
        String result = Bloco2.exercicio17_chegadaViagem(24,10,5,15);
        Assert.assertEquals(expected,result);
    }
    @Test
    public void exercicio17_chegadaViagem_Test4() {
        String expected = "Chegará às 7:49 do mesmo dia.";
        String result = Bloco2.exercicio17_chegadaViagem(5,0,2,49);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void exercicio18_tempoFimProcessamento_Test1() {
        String expected = "18:1:18";
        String result = Bloco2.exercicio18_tempoFimProcessamento(15,31,0,9000);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void exercicio19_calcSalario_Test1() {
        double expected = 300;
        double result = Bloco2.exercicio19_calcSalario(3);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio19_calcSalario_Test2() {
        double expected = 270;
        double result = Bloco2.exercicio19_calcSalario(0);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio19_calcSalario_Test3() {
        double expected = 395;
        double result = Bloco2.exercicio19_calcSalario(10);
        Assert.assertEquals(expected,result,0.001);
    }

    @Test
    public void exercicio20_aluguerKitJardinagem_Test1() {
        double expected = 34;
        double result = Bloco2.exercicio20_aluguerKitJardinagem("a",5,2);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio20_aluguerKitJardinagem_Test2() {
        double expected = 83.2;
        double result = Bloco2.exercicio20_aluguerKitJardinagem("b",1,6.6);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio20_aluguerKitJardinagem_Test3() {
        double expected = 43;
        double result = Bloco2.exercicio20_aluguerKitJardinagem("a",6,1.5);
        Assert.assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio20_aluguerKitJardinagem_Test4() {
        double expected = 161;
        double result = Bloco2.exercicio20_aluguerKitJardinagem("c",7,10.5);
        Assert.assertEquals(expected,result, 0.001);
    }
    @Test
    public void exercicio20_aluguerKitJardinagem_Test5() {
        double expected = 118;
        double result = Bloco2.exercicio20_aluguerKitJardinagem("c",5, 9);
        Assert.assertEquals(expected,result, 0.001);
    }
    @Test
    public void exercicio20_aluguerKitJardinagem_Test6() {
        double expected = 51;
        double result = Bloco2.exercicio20_aluguerKitJardinagem("b",3, 0.5);
        Assert.assertEquals(expected,result, 0.001);
    }
}
