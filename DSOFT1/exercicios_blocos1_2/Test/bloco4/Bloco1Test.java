package bloco4;

import org.junit.Assert;
import org.junit.Test;

public class Bloco1Test {

    @Test
    public void exercicio3_V2() {
        //arrange
        double raio = 5;
        double altura = 10;
        double expected = 785000;
        //act
        double result = Bloco1.exercicio3_V2(altura, raio);
        //assert
        Assert.assertEquals(expected, result, 0.00001);
    }

    @Test
    public void exercicio4_V2() {
        //arrange
        double intervaloSegundos = 10;
        double expected = 3400;
        //act
        double result = Bloco1.exercicio4_V2(intervaloSegundos);
        //assert
        Assert.assertEquals(expected, result, 0.0001);
    }

    @Test
    public void exercicio5_V2() {
        //arrange
        double tempo = 10;
        double expected = 490;
        //act
        double result = Bloco1.exercicio5_V2(tempo);
        //assert
        Assert.assertEquals(expected, result, 0.0001);
    }

    @Test
    public void exercicio6_V2() {
        double sombraEdificio = 20;
        double sombraPessoa = 2.1;
        double alturaPessoa = 1.6;
        double expected = 15;
        double result = Bloco1.exercicio6_V2(sombraEdificio, sombraPessoa, alturaPessoa);
        Assert.assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio7_V2() {
        double expected = 11326;
        double result = Bloco1.exercicio7_V2();
        Assert.assertEquals(expected, result, 0.0001);
    }

    @Test
    public void exercicio8_V2() {
        double a = 60;
        double b = 40;
        double expected = 52;

        double result = Bloco1.exercicio8_V2(a, b);
        Assert.assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio9_V2() {
        double a = 20;
        double b = 30;
        double expected = 100;

        double result = Bloco1.exercicio9_V2(a, b);
        Assert.assertEquals(expected, result, 0.0001);
    }

    @Test
    public void exercicio10_V2() {
        double c1 = 10;
        double c2 = 10;
        double expected = 14.14;

        double result = Bloco1.exercicio10_V2(c1, c2);
        Assert.assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio11_V2() {
        double x = 5;
        double expected = 9;
        double result = Bloco1.exercicio11_V2(x);
        Assert.assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio12_V2() {
        double celsius = 0;
        double expected = 32;
        double result = Bloco1.exercicio12_V2(celsius);
        Assert.assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio13() {
        int h =1;
        int m =0;
        int expected = 60;
        int result = Bloco1.exercicio13(h,m);
        Assert.assertEquals(expected,result);
    }
}